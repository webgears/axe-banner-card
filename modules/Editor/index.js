
import createGui from 'wg-engine-ui';
import 'wg-engine-ui/dist/plugins/functionality/export/register';

import createControlsExtension from './extensions/top-panel-controls';
import createUserComponentExtension from './extensions/user-component';

import TimeSystem from '../Systems/Time';

export default class Editor {
    constructor({ core }) {
        this.core = core;
        const { dom, scene, renderer, ecs } = core;

        createControlsExtension({ core });
        createUserComponentExtension({ core });

        const gui = createGui(dom, {
            renderer,
            onSetSize() {}
        });

        // Replace original renderer with gui renderer
        core.renderer = gui.renderer;

        const [timeSystem] = ecs.getSystems([TimeSystem]);
        timeSystem.timeScale = 0;

        window.gui = gui;

        gui.setScene(scene);
    }
}
