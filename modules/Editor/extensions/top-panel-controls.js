import React from 'react';
import { inject } from 'mobx-react';
import { IconButton } from 'wg-engine-ui/dist/view/atoms/IconButton/IconButton';
import { registerTopPanelElement, TopPanelElementPlacement } from 'wg-engine-ui/dist/plugins';
import InputSystem from '../../Systems/Input';
import OrbitControlSystem from '../../Systems/OrbitControlSystem';

export default function registerControls({ core: { ecs } }) {
    @inject('model')
    class ViewComponent extends React.Component {
        state = {
            useEditorControls: true
        };

        componentDidMount() {
            this.updateControls();
        }

        componentDidUpdate() {
            this.updateControls();
        }

        render() {
            const { useEditorControls } = this.state;

            return (
                <IconButton
                    icon='editor-controls'
                    title='Switch input system'
                    isActive={useEditorControls}
                    onClick={this.handleClick}
                />
            );
        }

        handleClick = () => {
            this.setState({ useEditorControls: !this.state.useEditorControls });
        }

        updateControls() {
            const { useEditorControls } = this.state;
            const [inputSystem, orbitControlSystem] = ecs.getSystems([InputSystem, OrbitControlSystem]);

            if (useEditorControls) {
                inputSystem.disable();
                orbitControlSystem.disable();
            } else {
                inputSystem.enable();
                orbitControlSystem.enable();
            }

            // Get Editor orbit controls
            // console.log(this.props.model.sceneHelpers.currentControls);
        }
    }

    registerTopPanelElement({
        placement: TopPanelElementPlacement.MIDDLE,
        viewComponent: ViewComponent,
        needSeparators: false
    });
}
