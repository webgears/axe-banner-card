import React from 'react';
import { registerComponent } from 'wg-engine-ui/dist/plugins';
import FieldListModel from 'wg-engine-ui/dist/model/FieldListModel';
import BaseComponentModel from 'wg-engine-ui/dist/plugins/components/Base/model/BaseComponentModel';
import FieldList from 'wg-engine-ui/dist/view/FieldList/FieldList';
import * as GFX from '../../wg';

function getShapeByComponentField(fieldName, fieldValue) {
    if (typeof fieldValue === 'boolean') {
        return [fieldName, 'bool'];
    }

    if (typeof fieldValue === 'number') {
        return [fieldName, 'number'];
    }

    if (typeof fieldValue === 'string') {
        return [fieldName, 'string'];
    }

    if (fieldValue instanceof GFX.Vector2) {
        return [fieldName, 'vec2'];
    }

    if (fieldValue instanceof GFX.Vector3) {
        return [fieldName, 'vec3'];
    }

    return [fieldName, ['unknown', { readOnly: true }]];
}

export default function registerUserComponent({ core }) {
    core.components.forEach((Component) => {
        class ViewComponent extends React.Component {
            state = {
            };

            componentDidMount() {
                this.props.componentAttachment.componentModel.enterLive();
            }

            render() {
                const model = this.props.componentAttachment.componentModel;

                return (
                    <FieldList fields={model.fieldList.structure} />
                );
            }
        }

        class Model extends BaseComponentModel {
            constructor(native, model) {
                super(native, model);

                const shape = Object.entries(native)
                    .map(([fieldName, fieldValue]) => getShapeByComponentField(fieldName, fieldValue));

                this.fieldList = FieldListModel.fromShape(this, model, shape);
            }

            read() {
                this.fieldList.read();
            }
        }

        const $type = Symbol.for('componentType');

        registerComponent(Component, {
            // sceneHelperComponent,
            modelClass: Model,
            viewComponent: ViewComponent,
            iconName: Component[$type]
        });
    });
}
