import { MaterialFactory } from '@webgears-app/webgl-renderer';
import { GLTFImporter } from '@webgears-app/gltf-importer';
import BaseCreator from './Base';

export default class GltfBundleCreator extends BaseCreator {
    static type = 'bundle';

    async load({ src }) {
        if (typeof src !== 'string') {
            throw new Error('You should pass src string to bundle creator');
        }

        const { vertexStaging, elementStaging } = this.manager.mem;

        const loader = new GLTFImporter({
            loader: this.loader,
            gltfFile: src,
            vertexStaging,
            elementStaging,
            materialFactory: new MaterialFactory(),
            config: {
                editor: false,
                // maxSceneNodes: maxInstancesCount
            }
        });

        return loader.assets;
    }

    parse(gltfAssets, config) {
        const { manager } = this;

        for (const scene of gltfAssets.scenes.values()) {
            const name = config.name || scene.name || scene.name;

            scene.name = name;
            manager.add('scene', name, scene);
        }

        for (const image of gltfAssets.images.values()) {
            manager.add('image', image.name, image);
        }

        for (const texture of gltfAssets.textures) {
            manager.add('texture', texture.name, texture);
        }

        for (const material of gltfAssets.materials) {
            manager.add('material', material.name, material);
        }

        // TODO Move gltf assets to our manager
        // console.log('parsed gltf', obj);
    }
}
