import { Texture, Material, TextureLevelData } from '@webgears-app/engine-core';
import { RGBELoader } from '@webgears-app/gltf-importer/lib/RGBELoader';
import { ConvolutionRoutine } from '@webgears-app/webgl-renderer';
import BaseCreator from './Base';

export default class EnvironmentCreator extends BaseCreator {
    constructor(config) {
        super(config);

        const { loader } = this.manager;
        this.loader = loader.createChild({ baseUrl: this.basePath, type: Image });
    }

    static type = 'environment';

    async load({ src }) {
        if (typeof src !== 'string') {
            throw new Error('You should pass src string to bundle creator');
        }

        const { loader } = this;

        try {
            const response = await new RGBELoader({ loader, src }).assets;
            return response;
        } catch (e) {
            return new Error(`Can't get "${this.type}" resource with src "${src}"`);
        }
    }

    async parse(data, normalizedConfig) {
        const { customImageData, maxEnvLuminance } = data;
        const { width, height, depth, format, internalFormat, componentType } = customImageData;
        const { LINEAR, LINEAR_MIPMAP_LINEAR } = Texture.Filter;
        const { name, luminance = 2048 } = normalizedConfig;

        const texture = new Texture({
            name,

            width,
            height,
            depth,

            bindingPoint: Texture.BindingPoint.TEXTURE_2D,

            minFilter: LINEAR_MIPMAP_LINEAR,
            magFilter: LINEAR,
            wrapS: Texture.WrappingMode.CLAMP_TO_EDGE,
            wrapT: Texture.WrappingMode.CLAMP_TO_EDGE,

            format,
            internalFormat,
            componentType,

            levelsData: [new TextureLevelData({ data: customImageData })],
            dynamic: false
        });

        const environmentMap = new Material.EnvironmentMapProperties(luminance, maxEnvLuminance, texture);

        const { type, manager } = this;
        const { renderer } = manager.core;
        const { vertexStaging, elementStaging } = manager.mem;

        const convolutionRoutine = new ConvolutionRoutine({
            renderer,
            vertexStaging,
            elementStaging,
            environmentMap,

            convolutionMapSize: 4,
            preFilteredEnvMapSize: 4,
            brdfMapSize: 4,
            samplesNumber: 4,
            samplingDelta: Math.PI / 4
        });

        // console.time('ibl');
        const [illuminanceMap, preFilteredEnvMap, brdfLUT] = await convolutionRoutine.assets;
        // console.timeEnd('ibl');

        const IBL = {
            environmentMap,
            luminance,
            maxEnvLuminance,
            illuminanceMap,
            preFilteredEnvMap,
            brdfLUT
        };

        manager.add(type, name, IBL);
        return IBL;
    }
}
