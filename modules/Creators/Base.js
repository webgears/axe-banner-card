export default class BaseCreator {
    constructor({ basePath = '', manager, type } = {}) {
        if (!type) {
            throw new Error('Type should be passed');
        }

        if (!manager) {
            throw new Error('Manager should be passed');
        }

        const { loader } = manager;

        this.manager = manager;
        this.basePath = basePath;
        this.loader = loader.createChild({ baseUrl: basePath });
        this.type = type;
    }

    normalize(config) {
        if (typeof config === 'string') {
            return {
                src: config
            };
        }

        return config;
    }

    async create(inConfig) {
        const normalizedConfig = this.normalize(inConfig);

        const data = normalizedConfig.src ?
            await this.load(normalizedConfig) :
            normalizedConfig;

        const result = await this.parse(data, normalizedConfig);
        return result;
    }

    async load({ src }) {
        if (typeof src !== 'string') {
            return new Error(`Loading "${this.type}" error: path must be a string`);
        }

        const { loader } = this;

        try {
            const response = await loader.load(src);
            return response;
        } catch (e) {
            return new Error(`Can't get "${this.type}" resource with src "${src}"`);
        }
    }

    parse(result, config) {
        const name = config.name || result.name || 'noname';

        this.manager.add(this.type, name, result);
        return result;
    }

    configure() {
        // TODO Do we need this?
    }
}
