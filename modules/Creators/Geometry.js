import * as GFX from '../wg';
import BaseCreator from './Base';

class Geometry {
    attributes = new Map();
    indices = null;
}

export default class GeometryCreator extends BaseCreator {
    static type = 'geometry';

    parse(obj) {
        const resManager = this.manager;
        const { type } = this;

        const geometry = new Geometry();

        let stride = 0;
        const data = {};
        for (const attr of obj.attr) {
            stride += attr.num;
            data[attr.name] = attr.data;
        }

        const vertexBuffer = resManager.mem.vertexStaging.allocMemory(Float32Array, stride * obj.attr[0].data.length / obj.attr[0].num);
        for (let i = 0; i < obj.attr[0].data.length / obj.attr[0].num; i++) {
            vertexBuffer[i * 6 + 0] = data.position[i * 3 + 0];
            vertexBuffer[i * 6 + 1] = data.position[i * 3 + 1];
            vertexBuffer[i * 6 + 2] = data.position[i * 3 + 2];
            vertexBuffer[i * 6 + 3] = data.normal[i * 3 + 0];
            vertexBuffer[i * 6 + 4] = data.normal[i * 3 + 1];
            vertexBuffer[i * 6 + 5] = data.normal[i * 3 + 2];
        }

        for (const attr of obj.attr) {
            const attribute = new GFX.VertexAttribute({
                componentType: GFX.DataType.FLOAT,
                countComponents: attr.num,
                countElements: attr.data.length / attr.num,
                offset: vertexBuffer.byteOffset + (attr.name === 'position' ? 0 : 12),
                semantic: GFX.AttributeSemantic.POSITION,
                stagingBuffer: vertexBuffer.buffer,
                elementType: attr.num === 3 ? GFX.VertexAttributeType.VEC3 : GFX.VertexAttributeType.VEC2,
                stride: stride * Float32Array.BYTES_PER_ELEMENT,
                type: Float32Array
            });

            if (attr.name === 'position') geometry.attributes.set(GFX.AttributeSemantic.POSITION, attribute);
            if (attr.name === 'normal') geometry.attributes.set(GFX.AttributeSemantic.NORMAL, attribute);
            if (attr.name === 'uv') geometry.attributes.set(GFX.AttributeSemantic.TEX_COORD_CHANNEL_0, attribute);
            geometry.indices = undefined;
        }

        if (obj.index) {
            geometry.indices = null; // TEMP!!!
        }

        resManager.add(type, obj.name, geometry);
        return geometry;
    }
}
