import * as GFX from '../../wg';

const $componentClasses = Symbol.for('componentClasses');

const $type = Symbol.for('componentType');
const $withUserComponentsScene = Symbol.for('withUserComponentsScene');
const $isEngineComponent = Symbol.for('isEngineComponent');

function getUserComponentsCloneSystem(componentsClassList = []) {
    return class UserComponentsCloneSystem extends GFX.System {
        static [$type] = 'UserComponentsCloneSystem';

        _copyFromScene(srcScene, copyContext = {}) {
            if (!srcScene[$withUserComponentsScene]) { return; }

            const { nodeToNode } = copyContext;

            for (const [srcNode, dstNode] of nodeToNode.entries()) {
                const components = srcNode.getComponents(componentsClassList);

                for (const component of components) {
                    if (!component) { continue; }

                    const newComponent = component.clone();

                    dstNode.attach(newComponent);
                    newComponent.owner = dstNode;
                }
            }
        }
    };
}

const engineSystems = [
    GFX.Animations,
    GFX.Cameras,
    GFX.CubeCameras,
    GFX.Lights,
    GFX.Meshes,
    GFX.Sprites,
    GFX.Transforms
];
const engineComponents = [
    { component: GFX.Light, storage: () => new GFX.DefaultComponentStorage(GFX.Light) },
    { component: GFX.LightShadowCaster, storage: () => new GFX.DefaultComponentStorage(GFX.LightShadowCaster) },
    { component: GFX.Mesh, storage: () => new GFX.DefaultComponentStorage(GFX.Mesh) },
    { component: GFX.Sprite, storage: () => new GFX.DefaultComponentStorage(GFX.Sprite) },
    { component: GFX.Visibility, storage: () => new GFX.DefaultComponentStorage(GFX.Visibility) },
    { component: GFX.Camera, storage: () => new GFX.DefaultComponentStorage(GFX.Camera) },
    { component: GFX.CubeCamera, storage: () => new GFX.DefaultComponentStorage(GFX.CubeCamera) },
    { component: GFX.Transform, storage: () => new GFX.TransformStorage() },
    { component: GFX.NodeAnimation, storage: () => new GFX.DefaultComponentStorage(GFX.NodeAnimation) },
    { component: GFX.Bone, storage: () => new GFX.DefaultComponentStorage(GFX.Bone) }
];

engineComponents.forEach(({ component }) => { component[$isEngineComponent] = true; });

GFX.Light[$type] = 'Light';
GFX.LightShadowCaster[$type] = 'LightShadowCaster';
GFX.Mesh[$type] = 'Mesh';
GFX.Sprite[$type] = 'Sprite';
GFX.Visibility[$type] = 'Visibility';
GFX.Camera[$type] = 'Camera';
GFX.CubeCamera[$type] = 'CubeCamera';
GFX.Transform[$type] = 'Transform';
GFX.NodeAnimation[$type] = 'NodeAnimation';
GFX.Bone[$type] = 'Bone';

// export function createEmptyScene({ name, components }) {
//     const scene = new GFX.Scene({});
//     scene.name = name;

//     return scene;
// }

// TODO HACK For v 7.0.0 for shadows to work
GFX.Meshes.prototype._updateDepthMaterial = function(instanceDataManager, subMesh) {
    const { material } = subMesh;
    const { castShadow } = material;

    if (castShadow) {
        if (subMesh._depthMaterial === null) {
            const { _renderer } = this;
            const { _depthMaterial } = _renderer;
            const { skeleton = null, instanceId } = subMesh;

            const batchRenderable = instanceDataManager.getInstanceRenderable(instanceId);

            if (skeleton === null) {
                subMesh._depthMaterial = _depthMaterial;
            } else if (this._skinnedDepthMaterials.has(skeleton)) {
                subMesh._depthMaterial = this._skinnedDepthMaterials.get(skeleton);
            } else {
                const depthMaterial = _depthMaterial.clone();

                // This line was commented
                // depthMaterial.name = `${subMesh.name}_submesh_${i}_depthmat`;

                subMesh._depthMaterial = depthMaterial;

                this._skinnedDepthMaterials.set(skeleton, depthMaterial);
            }

            batchRenderable.depthMaterial = subMesh._depthMaterial;
        }
    } else if (subMesh._depthMaterial !== null) {
        const { instanceId } = subMesh;

        const batchRenderable = instanceDataManager.getInstanceRenderable(instanceId);

        subMesh._depthMaterial = null;
        batchRenderable.depthMaterial = null;
    }
};

const storagesForComponents = new Map();

export function createEmptyScene({ name, systems, components, maxInstanceCount }) {
    const fullComponentList = [...engineComponents];
    let fullSystemsList;
    if (name === 'main') fullSystemsList = [...systems, ...engineSystems, getUserComponentsCloneSystem(components)];
    else fullSystemsList = [...engineSystems];

    for (const component of components) {
        if (storagesForComponents.has(component)) {
            const componentStructure = storagesForComponents.get(component);
            fullComponentList.push(componentStructure);
            continue;
        }

        const componentStructure = {
            component,
            storage: () => new GFX.DefaultComponentStorage(component)
        };
        fullComponentList.push(componentStructure);
        storagesForComponents.set(component, componentStructure);
    }

    const typesSet = new Set();
    for (const { component } of engineComponents) {
        if (!component[$type]) {
            throw new Error('Type not set for component');
        }

        if (typesSet.has(component[$type])) {
            throw new Error(`Type "${component[$type]}" already exists`);
        }

        typesSet.add(component[$type]);
    }

    const scene = new GFX.Scene({
        maxSceneNodes: maxInstanceCount || 64,
        name,
        config: {
            ecsConfig: {
                components: fullComponentList,
                systems: fullSystemsList
            }
        }
    });

    // for (const system of scene.ecs.getSystems()) {
    //     const originalEarlyUpdate = system.earlyUpdate;
    //     system.earlyUpdate = function(...args) {
    //         const eaname = system.constructor.name + '_earlyUpdate';
    //         const eastart = eaname + '_start';
    //         const eaend = eaname + '_end';
    //         performance.mark(eastart);
    //         const result = originalEarlyUpdate.apply(system, args);
    //         performance.mark(eaend);
    //         performance.measure(eaname, eastart, eaend);
    //         return result;
    //     };

    //     const originalLateUpdate = system.lateUpdate;
    //     system.lateUpdate = function(...args) {
    //         const ltname = this.constructor.name + '_lateUpdate';
    //         const ltstart = ltname + '_start';
    //         const ltend = ltname + '_end';
    //         performance.mark(ltstart);

    //         const result = originalLateUpdate.apply(this, args);

    //         performance.mark(ltend);
    //         performance.measure(ltname, ltstart, ltend);

    //         return result;
    //     };
    // }

    scene[$withUserComponentsScene] = true;
    scene[$componentClasses] = fullComponentList.reduce((result, { component }) => {
        result[component[$type]] = component;
        return result;
    }, {});

    return scene;
}
