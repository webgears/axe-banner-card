import { Mesh } from '@webgears-app/engine-core';
import { PbrMeshMaterial } from '@webgears-app/webgl-renderer';
import BaseCreator from '../Base';
import { getNodeByName, getNodeBySelector, mergeScene } from '../../Core/lib';
import { getComponentCreator, mergeComponentProperties } from './components';
import { createEmptyScene } from './creation';

const $componentClasses = Symbol.for('componentClasses');
const $type = Symbol.for('componentType');

export default class SceneCreator extends BaseCreator {
    static type = 'scene';

    async parse(config, externalConfig) {
        const { manager } = this;
        const creatingResources = [];
        const name = externalConfig.name || config.name || config.src || 'noname';

        const scene = createEmptyScene({
            name,
            components: manager.components,
            systems: manager.systems,
            maxInstanceCount: externalConfig.maxInstanceCount || config.maxInstanceCount
        });

        for (const type in config.resources || []) {
            for (const rs of config.resources[type]) {
                const creator = manager.creators.get(type);

                if (!creator) {
                    console.warn(`No creator for "${type}"`);
                    continue;
                }

                const creatingResource = creator.create(rs);
                creatingResources.push(creatingResource);
            }
        }

        await Promise.all(creatingResources);

        let useAsRootCount = 0;
        for (const attachConfig of config.attaches || []) {
            const attachName = attachConfig.name;
            const sceneName = attachConfig.scene;
            const useAsRoot = Boolean(attachConfig.useAsRoot);

            if (useAsRoot) {
                useAsRootCount++;
            }

            if (useAsRootCount > 1) {
                throw new Error('No more than 1 "useAsRoot" attach available');
            }

            if (!attachName) {
                throw new Error(`Wrong attach config: ${JSON.stringify(attachConfig)}`);
            }

            if (!sceneName) {
                scene.root.createChild({
                    name: attachName
                });
            } else {
                const bundleScene = manager.get('scene', sceneName);
                // const parentNode = attachConfig.parent ? getNodeByName(scene, attachConfig.parent) : scene.root;

                mergeScene(scene.root, bundleScene, {
                    name: attachName,
                    useAsRoot
                });
            }
        }

        const componentClassesByType = scene[$componentClasses];

        for (const { selector, components } of config.entities || []) {
            const node = selector ? getNodeBySelector(scene, selector) : scene.root;

            if (!node) {
                throw new Error(`Not found entity by selector "${selector}" in the game scene`);
            }

            for (const { type, ...settings } of components) {
                const ComponentClass = componentClassesByType[type];

                if (!ComponentClass) {
                    throw new Error(`Not found component type "${type}"`);
                }

                // Try to find this component if exists
                const [existingComponent] = node.getComponents([ComponentClass]);

                if (existingComponent) {
                    mergeComponentProperties(existingComponent, settings);
                } else {
                    const createComponent = getComponentCreator(ComponentClass);
                    createComponent(node, ComponentClass, settings);
                }
            }
        }

        scene.traverse((node) => {
            const [mesh] = node.getComponents([Mesh]);

            if (mesh) {
                for (const subMesh of mesh.subMeshes()) {
                    const { material } = subMesh;

                    material.castShadow = true;
                    material.receiveShadow = true;
                }
            }

            return true;
        });

        manager.add(this.type, name, scene);
        return scene;
    }
}
