import { Mesh, Sprite } from '@webgears-app/engine-core';

const $isEngineComponent = Symbol.for('isEngineComponent');
const $type = Symbol.for('componentType');

const visibilityComponentCreator = (node, Visibility, settings) => {
    const component = new Visibility();
    node.attach(component);

    const { visible = true } = settings;

    const [mesh, sprite] = node.getComponents([Mesh, Sprite]);
    if (mesh) {
        let index = 0;
        for (const sm of mesh.subMeshes()) { // TODO!!!
            component.setSubmeshVisibility(index, visible);
            index++;
        }
    } else if (sprite) {
        component.setSubmeshVisibility(0, visible);
    }
};

const defaultEngineComponentCreator = (node, ComponentClass, settings) => {
    const component = new ComponentClass(settings);
    node.attach(component);
};

const defaultUserComponentCreator = (node, ComponentClass, settings) => {
    const component = new ComponentClass({ ...settings, owner: node });
    node.attach(component);
};

function isPlainObject(obj) {
    if (typeof obj === 'object' && obj !== null) {
        if (typeof Object.getPrototypeOf === 'function') {
            const proto = Object.getPrototypeOf(obj);
            return proto === Object.prototype || proto === null;
        }

        return Object.prototype.toString.call(obj) === '[object Object]';
    }

    return false;
}

export function getComponentCreator(ComponentClass) {
    const type = ComponentClass[$type];
    const isEngineComponent = ComponentClass[$isEngineComponent];

    if (type === 'Visibility') {
        return visibilityComponentCreator;
    }

    if (isEngineComponent) {
        return defaultEngineComponentCreator;
    }

    return defaultUserComponentCreator;
}

export function mergeComponentProperties(component, settings) {
    for (const [key, newValue] of Object.entries(settings)) {
        if (!(key in component)) {
            throw new Error(`Property key "${key}" is not in component`);
        }

        const prevValue = component[key];
        if (prevValue && typeof prevValue === 'object' && isPlainObject(newValue)) {
            Object.assign(prevValue, newValue);
            continue;
        }

        component[key] = newValue;
    }
}
