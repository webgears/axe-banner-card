import * as GFX from '../wg';
import BaseCreator from './Base';

export default class MaterialCreator extends BaseCreator {
    static type = 'material';

    parse(obj) {
        const { type, manager } = this;
        const { core } = manager;

        const material = new GFX[obj.type]({ name: obj.name });

        if (obj.conf.albedo) material.albedo.copy(obj.conf.albedo);
        if (obj.conf.roughness) material.roughness = obj.conf.roughness;
        if (obj.conf.metalness) material.metalness = obj.conf.metalness;

        manager.add(type, obj.name, material);

        if (core.environment) {
            core.applyEnvironmentToMaterial(material);
        }

        return material;
    }
}
