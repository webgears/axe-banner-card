import * as GFX from '../wg';
import { packRGB } from '../Core/lib';
import OrbitControl from '../Components/OrbitControl';
import BaseCreator from './Base';

export default class PrefabCreator extends BaseCreator {
    static type = 'prefab';

    parse(data, normalizedConfig) {
        const { name: prefabName } = normalizedConfig;
        const { manager, type } = this;
        const { scene } = manager.core;
        const prefab = {};
        const {
            name = 'main',
            znear = 0.2,
            zfar = 200,
            aperture = 8,
            shutterSpeed = 0.008,
            ISO = 100,
            expComp = 2,
            perspectiveProjection,
            position = [0, 0, 10]
        } = data.camera || {};

        const { aspect = 1.2, yfov = 0.698 } = perspectiveProjection;

        const camera = new GFX.Camera({
            name,
            znear,
            zfar,
            aperture,
            shutterSpeed,
            ISO,
            expComp,
            perspectiveProjection: { aspect, yfov }
        });

        const cameraNode = scene.root.createChild({
            name: 'camera',
            position: new GFX.Vector3(...position)
        });

        cameraNode.attach(camera);

        prefab.camera = camera;
        prefab.cameraNode = cameraNode;

        if (data.orbitControl) {
            const orbitControl = new OrbitControl(data.orbitControl);

            cameraNode.attach(orbitControl);
            prefab.orbitControl = orbitControl;
        }

        const { directional, ambient, points } = data.lights || {};

        prefab.directionalLight = this._createDirectionalLight(scene, directional);
        prefab.ambientLight = this._createAmbientLight(scene, ambient);

        if (points && points.length) {
            points.forEach((point, i) => {
                prefab['point' + i] = this._createPointLight(scene, point);
            });
        }

        manager.add(type, prefabName, prefab);

        return prefab;
    }

    _createDirectionalLight(scene, settings = {}) {
        const {
            name = 'DirectionalLight',
            temperature = 5000,
            illuminance = 500000,
            colorFilter,
            orientation
        } = settings;

        const light = new GFX.DirectionalLight({
            name,
            temperature,
            illuminance
        });

        // const lightShadowCaster = new GFX.LightShadowCaster({
        //     castsShadow: true
        // });

        let color;
        if (colorFilter) {
            color = new GFX.RGBColor(...colorFilter.map(packRGB));
            light.setColorFilter(color);
        }

        const node = scene.root.createChild({
            orientation: orientation ? new GFX.Quaternion(...orientation) : undefined,
            position: new GFX.Vector3(0, 5, 0),
            name
        });

        node.attach(light);
        // node.attach(lightShadowCaster);
        return { light, node, color };
    }

    _createAmbientLight(scene, settings = {}) {
        const {
            name = 'AmbientLight',
            temperature = 8000,
            illuminance = 8000
        } = settings;

        const light = new GFX.AmbientLight({
            name,
            temperature,
            illuminance
        });

        const node = scene.root.createChild({ name });

        return { light, node, use: () => node.attach(light) };
    }

    _createPointLight(scene, settings = {}) {
        const {
            name = 'PointLight',
            temperature = 8000,
            illuminance = 8000,
            position = new GFX.Vector3(),
            colorFilter = [255, 255, 255]
        } = settings;

        const light = new GFX.PointLight({
            name,
            temperature,
            luminousPower: illuminance,
            colorFilter: new GFX.RGBColor(...colorFilter.map(packRGB))
        });

        const pos = new GFX.Vector3();
        pos.x = position.x;
        pos.y = position.y;
        pos.z = position.z;

        const node = scene.root.createChild({ name, position: pos });
        node.attach(light);

        return { light, node };
    }
}
