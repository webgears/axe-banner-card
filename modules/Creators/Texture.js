import { Texture, TextureLevelData, PixelDataFormat } from '@webgears-app/engine-core';
import path from 'path';
import BaseCreator from './Base';

export default class TextureCreator extends BaseCreator {
    constructor(config) {
        super(config);

        const { loader } = this.manager;
        this.loader = loader.createChild({ baseUrl: this.basePath, type: Image });
    }

    static type = 'texture';

    parse(image, normalizedConfig) {
        const { src, name } = normalizedConfig;
        const fileExt = path.extname(src).substr(1);

        const { width, height, depth, format, internalFormat, componentType } = image;
        const { LINEAR, LINEAR_MIPMAP_LINEAR } = Texture.Filter;
        const { REPEAT } = Texture.WrappingMode;
        const { RGBA, RGB } = PixelDataFormat;

        const texture = new Texture({
            name,
            width,
            height,
            depth,
            format: format || (fileExt === 'png' ? RGBA : RGB),
            internalFormat,
            wrapS: REPEAT,
            wrapT: REPEAT,
            componentType,
            bindingPoint: Texture.BindingPoint.TEXTURE_2D,
            levelsData: [new TextureLevelData({ data: image })],
            // levels: 10,
            minFilter: LINEAR_MIPMAP_LINEAR,
            magFilter: LINEAR
            // dynamic: true
        });

        const { type, manager } = this;
        manager.add(type, name, texture);

        return texture;
    }
}
