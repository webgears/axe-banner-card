import BaseCreator from './Base';

export default class FlatImageCreator extends BaseCreator {
    constructor(config) {
        super(config);

        const { loader } = this.manager;
        this.loader = loader.createChild({ baseUrl: this.basePath, type: Image });
    }

    static type = 'image';
}
