import * as GFX from '../wg';
import Resources from './Resources';
import Time from '../Systems/Time';
import Screen from '../Systems/Screen';

const $core = Symbol.for('core');
const $dom = Symbol.for('dom');
const $uiDom = Symbol.for('uiDom');
const $res = Symbol.for('res');
const $onscreen = Symbol.for('onscreen');

export default class Core {
    constructor({ dom, systems = [], components = [], creators = [] } = {}) {
        this.dom = dom;
        this.systems = systems;
        this.components = components;
        this.creators = creators;
    }

    async init() {
        const { dom, systems, components, creators } = this;

        this.res = new Resources({ core: this, systems, components, creators });
        const scene = await this.res.create('scene', { name: 'main', maxInstanceCount: this.res.mem.maxInstancesCount });

        this.ecs = scene.ecs;
        this.ecs.getSystems().forEach((system) => {
            if (typeof system.init === 'function') {
                system.init({ dom });
            }
        });
        this.ecs[$dom] = dom;
        this.ecs[$uiDom] = document.getElementById('app-ui');
        this.ecs[$res] = this.res;
        this.ecs[$core] = this;

        this.renderer = new GFX.Renderer({
            canvas: document.getElementById('app-canvas'),
            preserveDrawingBuffer: true,
            vertexStaging: this.res.mem.vertexStaging,
            elementStaging: this.res.mem.elementStaging,
            antialiasing: true,
            premultipliedAlpha: false,
            alpha: true,
            memoryConfig: {
                maxInstancesCount: this.res.mem.maxInstancesCount
            }
        });
        this.renderer.context.clearColor(0.5, 0.157, 0.565, 0);
        this.renderer.resize(window.innerWidth, window.innerHeight);

        this.renderPassInfo = new GFX.RenderPass({
            scene: null,
            camera: null,
            srgbOutput: true
        });

        this.renderPassInfo.scene = scene;

        this.scene = scene;
        this.camera = null;
        this.orbitControl = null;
        this.directionalLight = null;
        this.ambientLight = null;
        this.environment = null;

        this.mouse = GFX.Platform.desktop ? new GFX.DOMMouse(this.renderer.canvas) : null;
        this.touch = GFX.Platform.touchscreen ? new GFX.DOMTouchscreen(this.renderer.canvas) : null;

        this._addOnFrameHook();
    }

    _addOnFrameHook() {
        const systemsWithOnFrameStartHook = this.ecs.getSystems()
            .filter(system => typeof system.onFrameStart === 'function');
        const onSystemFrameStart = (system) => system.onFrameStart();

        const systemsWithOnFrameEndHook = this.ecs.getSystems()
            .filter(system => typeof system.onFrameEnd === 'function');
        const onSystemFrameEnd = (system) => system.onFrameEnd();

        this.renderer.onFrameStart.subscribe(() => {
            systemsWithOnFrameStartHook.forEach(onSystemFrameStart);
        });

        this.renderer.onFrameEnd.subscribe(() => {
            systemsWithOnFrameEndHook.forEach(onSystemFrameEnd);
        });
    }

    async useScene(scene) {
        this.scene.root.mergeCopy(scene);

        const prefab = this.res.getList('prefab')[0];

        this.camera = prefab.camera;
        this.orbitControl = prefab.orbitControl;
        this.directionalLight = prefab.directionalLight;
        this.ambientLight = prefab.ambientLight;

        this.renderPassInfo.camera = this.camera;

        if (this.res.hasType('environment')) {
            const environment = this.res.getList('environment')[0];
            await this.applyEnvironment(environment);
        } else {
            this.ambientLight.use();
        }
    }

    loop = async () => {
        performance.mark('Frame_start');
        const [time] = this.ecs.getSystems([Time]);

        if (this.ecs[$onscreen]) {
            this.ecs[$onscreen] = false;
            this.defferResize();
        }

        if (this.renderPassInfo.scene && this.renderPassInfo.camera) {
            this.renderPassInfo.dt = time.deltaTime;
            await this.renderer.render(this.renderPassInfo);
        }

        requestAnimationFrame(this.loop);
        performance.mark('Frame_end');
        performance.measure('Frame', 'Frame_start', 'Frame_end');
        performance.clearMarks();
        performance.clearMeasures();
    }

    run() {
        for (const system of this.ecs.getSystems()) {
            if (system.onStart) system.onStart();
        }

        this.loop();
    }

    defferResize() {
        clearTimeout(this.resizeTimeout);
        this.resizeTimeout = setTimeout(this.resize, 200);
    }

    resize = () => {
        const [screen] = this.ecs.getSystems([Screen]);
        const pr = Math.min(window.devicePixelRatio || 1, 2);

        this.renderer.resize(screen.width * pr, screen.height * pr);
        this.renderer.canvas.style.width = screen.width  + 'px';
        this.renderer.canvas.style.height = screen.height  + 'px';

        this.ecs[$uiDom].style.width = screen.width  + 'px';
        this.ecs[$uiDom].style.height = screen.height  + 'px';
    }

    applyEnvironment(environment) {
        const { illuminanceMap, preFilteredEnvMap, brdfLUT, luminance } = environment;
        const [lightSystem] = this.ecs.getSystems([GFX.Lights]);

        // TODO Remove it from here
        // lightSystem.shadowMapSize = 2048;
        lightSystem.splitLambda = 0.8;
        lightSystem.cascadesCount = 2;

        lightSystem.maxEnvLuminance = luminance;
        this.environment = environment;

        this.res.getList('material')
            .filter(mat => mat instanceof GFX.PbrMeshMaterial)
            .forEach(pbr => {
                pbr.illuminanceMap = illuminanceMap;
                pbr.preFilteredEnvMap = preFilteredEnvMap;
                pbr.brdfLUT = brdfLUT;
            });
    }

    applyEnvironmentToMaterial(material) {
        if (!this.environment) {
            // eslint-disable-next-line no-console
            console.warn(`Can't apply undefined environment to material "${material.name}"`);
            return;
        }

        if (Array.isArray(material)) {
            const materials = material;
            for (const oneMaterial of materials) {
                this.applyEnvMapToMaterial(oneMaterial);
            }
            return;
        }

        if (!(material instanceof GFX.PbrMeshMaterial)) {
            // eslint-disable-next-line no-console
            console.warn(`Can't apply environment map to material "${material.name}" - it must be PbrMeshMaterial`);
            return;
        }

        const { illuminanceMap, preFilteredEnvMap, brdfLUT } = this.environment;

        material.illuminanceMap = illuminanceMap;
        material.preFilteredEnvMap = preFilteredEnvMap;
        material.brdfLUT = brdfLUT;
    }
}
