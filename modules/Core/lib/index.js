import { Mesh, Sprite, Visibility, Transform } from '@webgears-app/engine-core';
import { PbrMeshMaterial, UnlitMeshMaterial } from '@webgears-app/webgl-renderer';

export mergeScene from './merge-scene';

const componentType = Symbol.for('componentType');

export function getAllComponentsMap(node, target = {}) {
    if (!node.scene) {
        return target;
    }

    return node.getAllComponents().reduce((acc, comp) => {
        acc[comp.constructor[componentType]] = comp;
        return acc;
    }, target);
}

const componentClasses = Symbol.for('componentClasses');

export function getComponentByName(node, compType) {
    if (!node.scene) return;

    const ComponentClass = node.scene[componentClasses][compType];

    if (!ComponentClass) return;

    return node.getComponents([ComponentClass])[0];
}

const componentCache = new WeakMap();

export function snapshotComponent(component) {
    componentCache.set(component, Object.assign({}, component));
}

export function restoreComponent(component, props) {
    Object.assign(component, componentCache.get(component), props);
}

export function componentHasSnapshot(component) {
    return componentCache.has(component);
}

export function getNodeByName(scene, name) {
    let resNode = null;

    scene.traverse((node) => {
        if (node.name === name) {
            resNode = node;
            return false;
        }
        return true;
    });

    return resNode;
}

export function getNodesByName(scene, name) {
    const foundNodes = [];

    scene.traverse((node) => {
        if (node.name === name) {
            foundNodes.push(node);
        }
        return true;
    });

    return foundNodes;
}

export function getNodesBySelector(rootNode, complexSelector) {
    const selectors = Array.isArray(complexSelector) ? complexSelector : [complexSelector];

    let searchNodes = [rootNode];

    selectors.forEach((name) => {
        // selector === name for now

        const parentSearchNodes = searchNodes;
        searchNodes = [];

        parentSearchNodes.forEach((searchNode) => {
            const foundNodes = getNodesByName(searchNode, name);
            searchNodes.push(...foundNodes);
        });
    });

    return searchNodes;
}

export function getNodeBySelector(rootNode, complexSelector) {
    const nodes = getNodesBySelector(rootNode, complexSelector);
    return nodes.length ? nodes[0] : null;
}

export function getNode(scene, cb) {
    const resNode = [];

    scene.traverse((node) => {
        if (cb(node)) {
            resNode.push(node);
        }
        return true;
    });

    return resNode;
}

export function cloneNode(sourceNode, parentNode, config = {}) {
    const [transform] = sourceNode.getComponents([Transform]);

    const innerConfig = {
        orientation: transform.orientation,
        position: transform.position,
        scale: transform.scale,
        ...config
    };

    const node = parentNode.createChild(innerConfig);

    const components = sourceNode.getAllComponents();
    for (const comp of components) {
        if (comp instanceof Transform) continue;

        if (comp instanceof Mesh) {
            const mesh = new Mesh({ name: comp.name });
            mesh.submeshes.push(...comp.submeshes);
            mesh.skeleton = comp.skeleton;
            node.attach(mesh);
            continue;
        }

        const newComp = comp.clone();
        newComp.owner = node;
        node.attach(newComp);
    }

    for (const child of sourceNode.children) {
        cloneNode(child, node);
    }

    return node;
}

export function setNodeVisibility(node, visibility = true) {
    if (!node.scene) return;
    node.traverse((tnode) => {
        const [mesh, sprite] = tnode.getComponents([Mesh, Sprite]);

        if (!mesh && !sprite) { return true; }

        let [visibilityComponent] = tnode.getComponents([Visibility]);

        if (!visibilityComponent) {
            visibilityComponent = new Visibility();
            tnode.attach(visibilityComponent);
        }

        if (mesh) {
            const { subMeshCount } = mesh;
            for (let i = 0; i < subMeshCount; i++) {
                visibilityComponent.setSubmeshVisibility(i, visibility);
            }
        } else if (sprite) {
            visibilityComponent.setSubmeshVisibility(0, visibility);
        }

        return true;
    });
}

export function dispose(instance) {
    Object.keys(instance).forEach((prop) => { instance[prop] = null; });
}

export function getMaterialsFromNode(node) {
    const materials = [];

    node.traverse((subNode) => {
        const [mesh] = subNode.getComponents([Mesh]);
        if (!mesh) return true;

        mesh.submeshes.forEach((submesh) => {
            materials.push(submesh.material);
        });

        return true;
    });

    return materials;
}

export function getFirstMaterial(node) {
    const [mesh] = node.getComponents([Mesh]);
    if (!mesh || !mesh.subMeshCount) {
        const [sprite] = node.getComponents([Sprite]);
        return sprite.material;
    }

    return mesh.getSubMesh(0).material;
}

export function cloneMaterial(material) {
    const props = {};
    let Class = PbrMeshMaterial;

    if (material instanceof UnlitMeshMaterial) {
        Class = UnlitMeshMaterial;
    }

    // eslint-disable-next-line guard-for-in
    for (const prop in material) {
        let propName = prop;
        let propValue = material[prop];

        if (prop[0] === '_') {
            propName = prop.slice(1);
            propValue = material[propName];
        }

        props[propName] = propValue;
    }

    props.name += '_copy';

    return new Class(props);
}

export function cloneNodeMaterials(node) {
    const materials = [];

    node.traverse((subNode) => {
        const [mesh] = subNode.getComponents([Mesh]);
        if (!mesh) return true;

        mesh.submeshes.forEach((submesh) => {
            const { material } = submesh;
            const materialClone = cloneMaterial(material);
            submesh.material = materialClone;
            materials.push(materialClone);
        });

        return true;
    });

    return materials;
}

// function allows to apply materials from a source node to other nodes
export function copyMaterialsToNodes(srcNode, ...targetNodes) {
    if (targetNodes.length > 1) {
        targetNodes.forEach((targetNode) => copyMaterialsToNodes(srcNode, targetNode));
        return;
    }

    const [targetNode] = targetNodes;
    const [srcMesh] = srcNode.getComponents([Mesh]);
    const [targetMesh] = targetNode.getComponents([Mesh]);

    if (!srcMesh || !targetMesh) return;

    const { submeshes: srcSubMeshes } = srcMesh;
    const { submeshes: targetSubMeshes } = targetMesh;

    srcSubMeshes.forEach((submesh, i) => {
        const targetSubMesh = targetSubMeshes[i];
        if (!targetSubMesh) return;
        targetSubMesh.material = submesh.material;
    });

    return true;
}

export const packRGB = v => v / 256;
export const unpackRGB = v => v * 256;
export const degToRad = Math.PI / 180;
export const radToDeg = 180 / Math.PI;

export function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}

export function deepClone(src) {
    const result = {};

    for (const prop in src) {
        const val = src[prop];

        if (typeof val === 'function') {
            result[prop] = val;
        } else if (Array.isArray(val)) {
            result[prop] = val.slice();
        } else if (typeof val === 'object') {
            result[prop] = deepClone(val);
        } else {
            result[prop] = val;
        }
    }

    return result;
}

export function getPropByPath(obj, path) {
    if (!Array.isArray(path)) return;

    return path.reduce((a, c) => a[c], obj);
}

export function setPropByPath(obj, path_, val) {
    if (!Array.isArray(path_)) return;

    const path = path_.slice();
    const propName = path.pop();

    const target = path.reduce((a, c) => a[c], obj);
    if (!target || !target[propName]) return;

    if (typeof val === 'function') {
        target[propName] = val(target[propName]);
    } else {
        target[propName] = val;
    }
}

export function getProbFromMap(probs) {
    const prob = Math.random();
    let prev = 0;

    for (const [key, value] of probs) {
        const cur = prev + value;

        if (prob >= prev && prob <= cur) {
            return key;
        }

        prev += value;
    }
}

export function clamp(value, from, to) {
    return Math.min(Math.max(value, from), to);
}
