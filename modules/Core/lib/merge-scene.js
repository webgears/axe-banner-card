import * as GFX from '../../wg';


function getHolder(parentNode, scene, config) {
    const name = config.name || scene.root.name;

    const [oldSceneTransform] = scene.root.getComponents([GFX.Transform]);
    const transformSettings = ['position', 'orientation', 'scale'].reduce((result, propertyName) => {
        result[propertyName] = (config[propertyName] || oldSceneTransform[propertyName]).clone();

        return result;
    }, {});

    const holder = !config.useAsRoot ? parentNode.createChild({}) : parentNode;

    holder.name = name;

    const [transform] = holder.getComponents([GFX.Transform]);
    Object.assign(transform, transformSettings);
    transform.needsUpdateLocalMatrix = true;
    transform.needsUpdateWorldMatrix = true;

    scene.root.getAllComponents().forEach((component) => {
        if (component instanceof GFX.Transform) { return; }

        holder.attach(component.clone());
    });

    return holder;
}

export default function mergeScene(parentNode, scene, config = {}) {
    const [{ animations }] = parentNode.scene.ecs.getSystems([GFX.Animations]);

    const holder = getHolder(parentNode, scene, config);
    holder.mergeCopy(scene);

    for (const an of animations) {
        an.stop();
    }

    return holder;
}
