import path from 'path';
import { Loader } from '@webgears-app/loader';
import * as GFX from '../wg';

const memoryMB = 32;

function generateNameForDuplicate(originalName, resources) {
    let name = originalName;
    let i = 1;
    while (name in resources) {
        name = `${originalName} (${i})`;
        i++;
    }
    return name;
}

export default class Resources {
    constructor({ core, basePath = 'assets', components, systems, creators } = {}) {
        this.res = {};
        this.creators = new Map();
        this.mem = {
            vertexStaging: new GFX.RangeBasedMemoryManager({ byteLength: memoryMB * 1024 * 1024 }),
            elementStaging: new GFX.RangeBasedMemoryManager({ byteLength: memoryMB * 1024 * 1024 }),
            maxInstancesCount: 2 ** 11
        };
        this.components = components;
        this.systems = systems;
        this.basePath = basePath;
        this.loader = new Loader({ baseUrl: path.join(basePath + '/'), type: Object });
        this.core = core;

        this.registerCreators(creators);
    }

    get(type, name) {
        if (!(type in this.res)) {
            throw new Error(`Type "${type}" doesn't exist`);
        }

        const resources = this.res[type];

        if (!name) {
            return resources;
        }

        if (!resources[name]) {
            throw new Error(`Resource "${name}" with type "${type}" doesn't exist`);
        }

        return resources[name];
    }

    getList(type) {
        if (!(type in this.res)) {
            throw new Error(`Type "${type}" doesn't exist`);
        }

        return Object.values(this.res[type]);
    }

    hasType(type) {
        return (type in this.res);
    }

    add(type, name, res) {
        if (!this.res[type]) this.res[type] = {};

        if (this.res[type][name]) {
            const newName = generateNameForDuplicate(name, this.res[type]);
            console.warn(`Resource "${name}" with type "${type}" already exists. Renamed to "${newName}"`);
            this.res[type][newName] = res;
            return;
        }

        this.res[type][name] = res;
    }

    async create(type, config) {
        const creator = this.creators.get(type);

        const resource = await creator.create(config);
        creator.configure(resource);

        return resource;
    }

    registerCreators(classes, options) {
        classes.forEach((Class) => this.registerCreator(Class.type, Class, options));
    }

    registerCreator(type, Class, options = { basePath: 'assets' }) {
        if (typeof type !== 'string') {
            throw new Error('First argument type should be a string');
        }

        if (typeof Class !== 'function') {
            throw new Error('Second argument should be a Class');
        }

        if (this.creators.has(type)) {
            throw new Error(`Type "${type}" is already set`);
        }

        const creator = new Class({
            ...this.config,
            ...options,
            manager: this,
            type
        });

        this.creators.set(type, creator);
    }
}
