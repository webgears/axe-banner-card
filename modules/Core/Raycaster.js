import { Raycaster as WGRaycaster } from '@webgears-app/raycaster';
import { Visibility } from '@webgears-app/engine-core';
import { Mouse } from '@webgears-app/input';
import { Vector2 } from '@webgears-app/math';

export const $isRaycastingIgnoreNode = Symbol('isRaycastingIgnoreNode');

export default class Raycastser {
    _hoverSubs = [];
    _hoverAnySubs = [];
    _clickSubs = [];
    _mouseEventSubs = [];
    _hovered = null;
    _isHoveredAny = null;
    _raycaster = new WGRaycaster({
        nearestOnly: true,
        filterNode: this._filterNode
    });

    constructor({ mouse, touch, camera, renderer, scene }) {
        this._camera = camera;
        this._renderer = renderer;
        this._scene = scene;

        this._addListeners(mouse, touch);
    }

    useCamera(camera) {
        this._camera = camera;
    }

    useScene(scene) {
        this._scene = scene;
    }

    _addListeners(mouse, touch) {
        const { canvas } = this._renderer;
        const { parentNode } = canvas;
        const mousePos = new Vector2();

        if (mouse) {
            // mouse.onMove.subscribe(() => this._handleMove(mouse.position));
            // mouse.onButtonRelease(Mouse.Buttons.LEFT)
            //     .subscribe(() => this._handleClick(mouse.position));

            parentNode.addEventListener('click', (e) => {
                this._handleClick(new Vector2(e.clientX, e.clientY));
            });

            parentNode.addEventListener('mouseup', () => this._handleMouseUp());

            parentNode.addEventListener('mousedown', (e) => {
                mousePos.set(e.clientX, e.clientY);
                this._handleMouseDown(mousePos);
            });

            parentNode.addEventListener('mousemove', (e) => {
                mousePos.set(e.clientX, e.clientY);
                this._handleMouseMove(mousePos);
            });
        }

        if (touch) {
            parentNode.addEventListener('touchend', () => {
                this._handleMouseUp();
            });

            parentNode.addEventListener('touchstart', (e) => {
                const [te] = e.touches;
                this._handleMouseDown(new Vector2(te.clientX, te.clientY));
            });

            parentNode.addEventListener('touchmove', (e) => {
                const [te] = e.touches;
                mousePos.set(te.clientX, te.clientY);
                this._handleMouseMove(mousePos);
            });
        }
    }

    _addCallback(nodeList, subs, callbacks) {
        subs.push({
            raycasted: false,
            nodeSet: new Set(Array.isArray(nodeList) ? nodeList : [nodeList]),
            callbacks: [].concat(callbacks)
        });
    }

    onHover(nodeList, callback) {
        this._addCallback(nodeList, this._hoverSubs, callback);
    }

    onHoverAny(callback) {
        this._addCallback([], this._hoverAnySubs, callback);
    }

    onClick(nodeList, callback) {
        this._addCallback(nodeList, this._clickSubs, callback);
    }

    onMouseEvent(nodeList, { mousedown, mouseup, mousemove }) {
        const callbacks = [];

        if (mousedown) callbacks.push(mousedown);
        if (mouseup) callbacks.push(mouseup);
        if (mousemove) callbacks.push(mousemove);

        this._addCallback(nodeList, this._mouseEventSubs, callbacks);
    }

    _filterNode(node) {
        if (node[$isRaycastingIgnoreNode]) { return false; }

        const [visibility] = node.getComponents([Visibility]);
        if (visibility && !visibility.isSubmeshVisible(0)) {
            return false;
        }

        return true;
    }

    _raycast(position) {
        const { width, height } = this._renderer;

        const ndc = new Vector2(
            (position.x / width) * 2 - 1,
            ((height - position.y) / height) * 2 - 1
        );

        const [object] = this._raycaster.castFromCamera(this._scene.root, ndc, this._camera);

        if (!object || !object.renderable) { return; }

        return object.node;
    }

    _handleMove(position) {
        if (!this._hoverSubs.length) { return; }

        const node = this._raycast(position);
        // TODO node is null

        if (this._hovered === node) { return; }
        this._hovered = node;

        let isHoveredAny = false;

        this._hoverSubs.forEach((sub) => {
            if (!sub.nodeSet.has(node)) {
                if (sub.isHovered) {
                    sub.callbacks[0](null);
                }

                sub.isHovered = false;
                return;
            }

            isHoveredAny = true;
            sub.isHovered = true;
            sub.callbacks[0](node);
        });

        if (this._isHoveredAny === isHoveredAny) { return; }
        this._isHoveredAny = isHoveredAny;

        this._hoverAnySubs.forEach(({ callbacks }) => {
            callbacks[0](isHoveredAny ? node : null);
        });
    }

    _handleMouseDown(position) {
        this._handlClickCommon(this._mouseEventSubs, position);
    }

    _handleMouseUp() {
        if (!this._mouseEventSubs.length) { return; }

        this._mouseEventSubs.forEach((subscriber) => {
            const { callbacks, raycasted } = subscriber;

            if (raycasted) {
                callbacks[1]();
                subscriber.raycasted = false;
            }
        });
    }

    _handleMouseMove(position) {
        if (!this._mouseEventSubs.length) { return; }

        this._mouseEventSubs.forEach((subscriber) => {
            const { callbacks, raycasted } = subscriber;

            if (raycasted) {
                callbacks[2](position);
            }
        });
    }

    _handleClick(position) {
        this._handlClickCommon(this._clickSubs, position);
    }

    _handlClickCommon(subList, position) {
        if (!subList.length) { return; }

        const node = this._raycast(position);
        // TODO node is null

        subList.forEach((subscriber) => {
            const { callbacks, nodeSet } = subscriber;

            if (!nodeSet.has(node)) {
                subscriber.raycasted = false;
                return;
            }

            subscriber.raycasted = true;
            callbacks[0](node, position);
        });
    }
}
