import Core from './Core/Core';

import Time from './Systems/Time';
import Input from './Systems/Input';
import Screen from './Systems/Screen';
import GameSystem from './Systems/GameSystem';
import UISystem from './Systems/UISystem';
import OrbitControlSystem from './Systems/OrbitControlSystem';
// import MovementSystem from './Systems/MovementSystem';
// import RotationSystem from './Systems/RotationSystem';

import OrbitControl from './Components/OrbitControl';
// import Movement from './Components/Movement';
// import Rotation from './Components/Rotation';

import GeometryCreator from './Creators/Geometry';
import MaterialCreator from './Creators/Material';
import FlatImageCreator from './Creators/FlatImage';
import SceneCreator from './Creators/SceneCreator';
import GltfCreator from './Creators/GltfBundle';
import TextureCreator from './Creators/Texture';
import EnvironmentCreator from './Creators/Environment';
import PrefabCreator from './Creators/PrefabCreator';

// import * as LIB from './Core/lib';

export default class App {
    constructor(dom) {
        this.dom = dom;
        this.init();

        this.isEditorMode = window.location.hash === '#gui';
    }

    async init() {
        this.core = new Core({
            dom: this.dom,
            systems: [
                Time,
                Input,
                Screen,
                GameSystem,
                OrbitControlSystem,
                UISystem
            ],
            components: [
                OrbitControl
            ],
            creators: [
                GeometryCreator,
                MaterialCreator,
                FlatImageCreator,
                SceneCreator,
                GltfCreator,
                TextureCreator,
                EnvironmentCreator,
                PrefabCreator
            ]
        });

        await this.core.init();

        const level1 = await this.core.res.create('scene', './scenes/level1.json');

        await this.core.useScene(level1);

        this.core.run();

        if (this.isEditorMode) {
            import('./Editor').then((module) => {
                const Editor = module.default;

                this.editor = new Editor({
                    core: this.core
                });
            });
        }
    }

    static create(dom) { return new App(dom); }
}
