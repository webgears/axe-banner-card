import * as React from 'react';
// import cn from 'classnames';
import styles from './index.css';

export default class DragArrow extends React.PureComponent {
    render() {
        const { visible } = this.props;

        return (
            <div className={styles.root} style={{ opacity: visible ? 1 : 0 }}></div>
        );
    }
}
