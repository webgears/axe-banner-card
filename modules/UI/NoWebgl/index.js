import React from 'react';
import styles from './index.scss';

export default class NoWebgl extends React.Component {
    render() {
        return (
            <div className={styles.root}>
                Не удалось инициализировать <a href="https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API">WebGL</a>. Возможно, Ваш браузер его не поддерживает.
            </div>
        );
    }
}
