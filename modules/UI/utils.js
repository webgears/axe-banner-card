export function zeroPad(num, size = 2) {
    let result = num + '';
    while (result.length < size) {
        result = '0' + result;
    }
    return result;
}

export function formatNumber(value) {
    return String(Math.floor(value)).split('').reverse()
        .reduce((a, c, i, { length }) => {
            const n = i + 1;
            return (n > 1 && n < length && !(n % 3) ? ' ' : '') + c + a;
        }, '');
}
