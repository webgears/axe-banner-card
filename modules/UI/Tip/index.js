import * as React from 'react';
import cn from 'classnames';
import styles from './index.css';

export default class Tip extends React.PureComponent {
    render() {
        const { visible, children } = this.props;
        if (!visible) return null;

        return (
            <div className={styles.root}>{children}</div>
        );
    }
}
