import * as React from 'react';
// import NoWebgl from './NoWebgl';
import DragButton from './DragButton';
import DragArrow from './DragArrow';
import SlideButton, { DIRECTION } from './SlideButton';
// import Tip from './Tip';

export default class UI extends React.PureComponent {
    render() {
        // console.log('render ui');
        const {
            offsetX = 0, opacity, isDragging, isActive,
            onTap, onTapEnd, onDrag, onSlideLeft, onSlideRight
        } = this.props;

        return (
            <React.Fragment>
                {/* <Tip visible={!mobile}>Use icon <br/> drag to cart</Tip> */}
                <DragArrow visible={isDragging} />
                <DragButton
                    inactive={!isActive}
                    visible={true}
                    onTap={onTap}
                    onTapEnd={onTapEnd}
                    onDrag={onDrag}
                    offsetX={offsetX}
                    opacity={opacity}
                >Swipe to cart</DragButton>
                <SlideButton
                    visible={!isDragging && isActive}
                    direction={DIRECTION.LEFT}
                    onClick={onSlideLeft}
                />
                <SlideButton
                    visible={!isDragging && isActive}
                    direction={DIRECTION.RIGHT}
                    onClick={onSlideRight}
                />
            </React.Fragment>
        );
    }
}
