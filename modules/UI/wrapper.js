import * as React from 'react';
import { hot } from 'react-hot-loader/root';
import UI from './index';

class UIWrapper extends React.Component {
    rafId = null;

    componentDidMount() {
        this.loop();
    }

    componentWillUnmount() {
        cancelAnimationFrame(this.rafId);
    }

    render() {
        return React.createElement(UI, this.props.store);
    }

    loop = () => {
        this.forceUpdate();
        this.rafId = requestAnimationFrame(this.loop);
    }
}

export default hot(UIWrapper);
