import * as React from 'react';
import cn from 'classnames';
import { clamp } from '../../Core/lib';
import styles from './index.css';

export default class DragButton extends React.PureComponent {
    state = {
        active: false,
        offsetX: 0
    }

    _srcElement = null;
    _rootOffsetX = 0;
    _rootWidth = 0;
    _buttonSize = 0;

    static getDerivedStateFromProps(nextProps, prevState) {
        const { offsetX } = nextProps;

        if (!prevState.active) {
            return { offsetX };
        }

        return null;
    }

    componentDidMount() {
        window.addEventListener('touchmove', this._hanldeMove);
        window.addEventListener('mousemove', this._hanldeMove);
        window.addEventListener('mouseup', this._hanldeTapEnd);
        window.addEventListener('touchend', this._hanldeTapEnd);
    }

    render() {
        // console.log('render drag button');
        const { visible, children = 'click', opacity = 1, inactive } = this.props;
        if (!visible) return null;

        const { offsetX, active } = this.state;

        const transformStyle = {
            transform: `translateX(${offsetX * this._rootWidth}px)`,
            transition: (!active ? 0.5 : 0) + 's'
        };

        const rootClassName = cn(styles.root, {
            [styles.inactive]: inactive,
            [styles.incart]: offsetX === 1
        });

        return (
            <div ref={this._saveRootParams} className={rootClassName}>
                <button
                    className={styles.button}
                    style={transformStyle}
                    onTouchStart={this._handleTapStart}
                    onMouseDown={this._handleTapStart}
                ></button>
                <div className={styles.overlay} style={transformStyle}></div>
                <div className={styles.body} style={{ opacity }}>
                    <div className={styles.arrows}></div>
                    <span className={styles.text}>{children}</span>
                    <div className={styles.arrows}></div>
                </div>
            </div>
        );
    }

    _moveButton(offsetX) {
        if (!this.state.active) return;

        const { onDrag } = this.props;

        if (typeof onDrag === 'function') {
            onDrag(offsetX);
        }

        this.setState({ offsetX });
    }

    _hanldeMove = (e) => {
        const { clientX } = e.touches ? e.touches[0] : e;
        const offsetX = this._calcOffset(clientX);

        this._moveButton(offsetX);
    }

    _handleTapStart = () => {
        const { onTap } = this.props;

        if (typeof onTap === 'function') {
            onTap();
        }

        this.setState({ active: true });
    }

    _hanldeTapEnd = () => {
        const { onTapEnd, inactive } = this.props;
        if (inactive) return;

        if (typeof onTapEnd === 'function') {
            onTapEnd();
        }

        this.setState({ active: false });
    }

    _calcOffset(absOffetX) {
        return clamp((absOffetX - this._rootOffsetX) / this._rootWidth, 0, 1);
    }

    _saveRootParams = (element) => {
        const { offsetWidth, offsetHeight } = element;

        this._buttonSize = offsetHeight / 2;
        this._rootWidth = offsetWidth - offsetHeight;
        this._rootOffsetX = element.getBoundingClientRect().left + this._buttonSize;
    }
}
