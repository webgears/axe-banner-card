import * as React from 'react';
import cn from 'classnames';
import styles from './index.css';

export const DIRECTION = {
    LEFT: 'left',
    RIGHT: 'right'
};

export default class SlideButton extends React.PureComponent {
    render() {
        const { onClick, direction, visible = true } = this.props;

        const rootClass = cn(styles.root, styles[direction], {
            [styles.visible]: visible
        });

        return (
            <button className={rootClass} onClick={onClick}></button>
        );
    }
}
