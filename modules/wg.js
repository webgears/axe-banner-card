import RangeBasedMemoryManager from '@webgears-app/host-memory-manager';
import { Vector3 } from '@webgears-app/math';
import { Transform, SceneNode, NodeAnimation } from '@webgears-app/engine-core';

export { RangeBasedMemoryManager };
export * from '@webgears-app/procedural-geometry';
export * from '@webgears-app/engine-core';
export * from '@webgears-app/webgl-renderer';
export * from '@webgears-app/math';
export * from '@webgears-app/engine-core/src/transforms/transformStorage';
export * from '@webgears-app/input';
export Platform from '@webgears-app/platform';

// Monkey patching
function mutateGFXComponents() {
    Object.defineProperty(Transform.prototype, 'needsUpdate', {
        set(value) {
            this.needsUpdateLocalMatrix = value;
            this.needsUpdateWorldMatrix = value;
        }
    });

    Object.defineProperty(Transform.prototype, 'worldPosition', {
        get() {
            return new Vector3(
                this.worldMatrix._30,
                this.worldMatrix._31,
                this.worldMatrix._32
            );
        }
    });

    const originalAttach = SceneNode.prototype.attach;
    SceneNode.prototype.attach = function(component) {
        const result = originalAttach.call(this, component);
        component.owner = this;
        return result;
    };

    const $savedUpdater = Symbol('savedUpdater');
    NodeAnimation.Channel.prototype.disable = function() {
        if (!this[$savedUpdater]) {
            this[$savedUpdater] = this._updater;
            this.update = () => {};
        }
    };

    NodeAnimation.Channel.prototype.enable = function() {
        if (this[$savedUpdater]) {
            this.update = this[$savedUpdater];
            this[$savedUpdater] = null;
        }
    };
}
mutateGFXComponents();
