import * as GFX from '../wg';

export default class BaseComponent extends GFX.Component {
    constructor(options = {}) {
        super(options);

        this.owner = options.owner;
    }

    clone() {
        throw new Error('Not implemented');
    }
}
