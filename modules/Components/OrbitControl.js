import * as GFX from '../wg';
import BaseComponent from './Base';

const $type = Symbol.for('componentType');

export default class OrbitControl extends BaseComponent {
    static [$type] = 'OrbitControl';

    constructor(conf) {
        super(conf);

        this.polar = conf.polar;
        this.azimuthal = conf.azimuthal;
        this.distance = conf.distance;
        this.forward = new GFX.Vector3();
        this.speed = conf.speed;
        this.needsUpdate = true;
        this.afrozen = conf.afrozen !== undefined ? conf.afrozen : false;
        this.pfrozen = conf.pfrozen !== undefined ? conf.pfrozen : false;
        this.target = new GFX.Vector3();
        this.center = new GFX.Vector3();
        this.autoRotate = Boolean(conf.autoRotate);
        if (conf.target) this.target.copy(conf.target);
        if (conf.center) this.center.copy(conf.center);
    }

    clone() {
        return new OrbitControl({
            polar: this.polar,
            azimuthal: this.azimuthal,
            distance: this.distance,
            afrozen: this.afrozen,
            pfrozen: this.pfrozen,
            speed: this.speed,
            target: this.target,
            center: this.center,
            autoRotate: this.autoRotate
        });
    }
}
