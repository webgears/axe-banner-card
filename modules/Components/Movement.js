import * as GFX from '../wg';
import BaseComponent from './Base';

const $type = Symbol.for('componentType');

export default class Movement extends BaseComponent {
    static [$type] = 'Movement';

    constructor(config) {
        super(config);

        const {
            direction = { x: 0, y: 0, z: 0 },
            speed = 0,
            linear = true,
            maxOffsetX = 10
        } = config;

        this.direction = new GFX.Vector3().copy(direction);
        this.speed = speed;
        this.linear = linear;
        this.maxOffsetX = maxOffsetX;
    }

    clone() {
        return new Movement(this);
    }
}
