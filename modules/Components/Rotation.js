import * as GFX from '../wg';
import BaseComponent from './Base';

const $type = Symbol.for('componentType');

export default class Rotation extends BaseComponent {
    static [$type] = 'Rotation';

    constructor(conf) {
        super(conf);

        this.axis = new GFX.Vector3();
        this.axis.copy(conf.axis);
        this.angle = conf.angle;
    }

    clone() {
        return new Rotation({
            axis: this.axis,
            angle: this.angle
        });
    }
}
