import * as GFX from '../wg';

export default class BaseSystem extends GFX.System {
    onStart() {}

    earlyUpdate() {}

    lateUpdate() {}

    _copyFromScene() {}

    _getComponentsMix(componentClasses) {
        if (!componentClasses.length) { return []; }

        const mask = componentClasses.reduce((result, Component) => result | Component.mask, 0);
        const enitites = [...this._ecs.getEntities(mask)];

        return enitites.map(entity => this._ecs.getEntityComponents(entity, componentClasses));
    }

    _getFirstComponentsMix(componentClasses) {
        return this._getComponentsMix(componentClasses)[0] || [];
    }
}
