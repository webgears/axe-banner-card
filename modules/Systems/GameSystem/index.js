/* global TWEEN */

import * as GFX from '../../wg';
import * as LIB from '../../Core/lib';
import BaseSystem from '../BaseSystem';
import OrbitControl from '../../Components/OrbitControl';
// import Raycaster from '../../Core/Raycaster';
import OrbitControlSystem from '../OrbitControlSystem';
import UISystem from '../UISystem';
// import { Quaternion, Vector3 } from '@webgears-app/math/lib';

const $res = Symbol.for('res');
const compArr = [GFX.Transform, GFX.Camera];
const compOrb = [GFX.Transform, OrbitControl];

const startCPos = new GFX.Vector3(-0.62, 0.9, 0.8);
const startCRot = new GFX.Quaternion(1, 0, 0, 0);
const endCPos = new GFX.Vector3(-0.8, 2, 1.12);
const endCRot = new GFX.Quaternion(0.8761871026898446, 0.41872872475525763, 0.21534091622919477, 0.10291115557791766);

endCPos.multiply(1.1);

const startAPos = new GFX.Vector3(-0.6, 0.9, 0.11103);
const startARot = new GFX.Quaternion(1, 0, 0, 0);
const endAPos = new GFX.Vector3(-0.0361191183328629, 0.373729318380356, 0.111032396554947);
const endARot = new GFX.Quaternion(0.70643, 0.70643, -0.03084, 0.03084);

const startPPos = new GFX.Vector3(-0.68, 1, 0.01103);
const $core = Symbol.for('core');

const getTransformUpdater = (transform) => () => {
    transform.needsUpdateLocalMatrix = true;
    transform.needsUpdateWorldMatrix = true;
};

const wait = (time) => new Promise((resolve) => setTimeout(resolve, time));

export default class GameSystem extends BaseSystem {
    constructor(ecs) {
        super(ecs);

        this._time = 0;
        this.progress = 0;
    }

    onStart() {
        const resources = this._ecs[$res];

        this._resources = resources;
        this._colorIndex = 0;
        this._colorMaps = [
            {
                albedoMap: resources.get('texture', 'map.jpg'),
                metalnessRoughnessMap: resources.get('texture', 'map_rm.jpg')
            },
            {
                albedoMap: resources.get('texture', 'map2.jpg'),
                metalnessRoughnessMap: resources.get('texture', 'map2_rm.jpg')
            }
        ];

        this._scene = resources.get('scene', 'main');
        this._axe = LIB.getNodeByName(this._scene, 'axe_spray_body');
        this._plane = LIB.getNodeByName(this._scene, 'Plane001');
        this._camera = LIB.getNodeByName(this._scene, 'camera');

        [this._axe_tr, this._axe_or] = this._axe.getComponents([GFX.Transform, OrbitControl]);
        [this._plane_tr] = this._plane.getComponents(compArr);
        [this._camera_tr, this._cameraComponent] = this._camera.getComponents(compArr);

        this._initialPosition = new GFX.Vector3(-0.02, 0, 0.8);
        this._initialOrientation = new GFX.Quaternion(1, 0, 0, 0);

        this._camera_tr.scale.set(1, 1, 1);
        this._camera_tr.position.copy(startCPos);
        this._camera_tr.orientation.copy(startCRot);
        this._camera_tr.needsUpdate = true;

        this._plane_tr.scale.set(140, 140, 140);
        this._plane_tr.position.copy(startPPos);
        this._plane_tr.orientation.set(1, 0, 0, 0);
        this._plane_tr.needsUpdate = true;

        this._axe_or.target.copy(startAPos);
        this._axe_tr.orientation.copy(startARot);
        this._axe_tr.needsUpdate = true;

        [this._orbitControlsSystem, this._uiSystem, this._animationSystem] = this._ecs.getSystems([OrbitControlSystem, UISystem, GFX.Animations]);

        this._axeProgressLerper = new GFX.Vector3Lerper(startAPos, endAPos);
        this._axeQuaternionLerper = new GFX.QuaternionLerper(startARot, endARot);

        // Fuke it, take first animation
        [this._dropAnimation] = this._animationSystem.animations;
        this._isDropped = false;

        /** hand point */
        // const {
        //     node: handPointNode,
        //     component: handPointComponent,
        //     material: handPointMaterial
        // } = this._createHandPoint();

        // this._handPointNode = handPointNode;
        // this._handPointComponent = handPointComponent;
        // this._handPointMaterial = handPointMaterial;

        // const { renderer, mouse, touch } = this._ecs[$core];

        // this._raycaster = new Raycaster({
        //     renderer,
        //     camera: this._cameraComponent,
        //     scene: this._scene,
        //     mouse,
        //     touch
        // });

        // this._tapOffsetX = 0;
        // this._raycaster.onMouseEvent(this._handPointNode, {
        //     mousedown: (node, mousePos) => {
        //         this.moveToBasketView();
        //         this._tapOffsetX = mousePos.x;
        //     },
        //     mouseup: () => this.tryDrop(),
        //     mousemove: (mousePos) => {
        //         this.progress = this._calcProgress(mousePos);
        //     }
        // });
    }

    tryDrop() {
        if (this._isDropped) return;

        if (this.progress >= 0.7) {
            this.progress = 1;
            this.animateBasketDrop();
            this._uiSystem.setStore({ isActive: false });

            return true;
        }

        this.progress = 0;
        this.moveToInitial();

        return false;
    }

    // _calcProgress(position) {
    //     return LIB.clamp((position.x - this._tapOffsetX) / (window.innerWidth - this._tapOffsetX), 0, 1);
    // }

    async moveToInitial() {
        TWEEN.removeAll();

        await Promise.all([
            this.moveCameraTo(startCPos, startCRot),
            this.moveTo(this._axe_tr.position, this._axeProgressLerper.start, { onUpdate: getTransformUpdater(this._axe_tr) }),
            this.moveTo(this._axe_tr.orientation, this._axeQuaternionLerper.start, { onUpdate: getTransformUpdater(this._axe_tr) }),
            this.togglingCaption(true)
            // this.togglingHandPoint(true),
        ]);

        this._orbitControlsSystem.enable();
    }

    async moveToBasketView() {
        TWEEN.removeAll();
        this._orbitControlsSystem.disable();
        this._axeProgressLerper.start = this._axe_tr.position.clone();
        this._axeQuaternionLerper.start = this._axe_tr.orientation.clone();

        await Promise.all([
            this.moveCameraTo(endCPos, endCRot),
            this.moveTo(this._axe_tr.orientation, endARot, { onUpdate: getTransformUpdater(this._axe_tr) }),
            this.togglingCaption(false)
            // this.togglingHandPoint(false)
        ]);
    }

    async animateBasketDrop() {
        TWEEN.removeAll();

        this._isDropped = true;

        const time = (1 - this.progress) * 700;

        await this.moveTo(this._axe_tr.position, endAPos, { onUpdate: getTransformUpdater(this._axe_tr), time });

        this._dropAnimation.play();

        await this.moveCameraTo({ x: -0.15697462362287623, y: 1.726058247426663, z: 1.0189203819703163 }, { re: 0.8671673897378462, imX: 0.49212873569984267, imY: 0.06640608087621534, imZ: 0.037686311790706244 });

        await wait(2000);
        window.location = 'https://2cart.net/03500d5';
    }

    async togglingCaption(isAppering = false) {
        const material = LIB.getFirstMaterial(this._plane);

        await this.moveTo(material, { opacity: isAppering ? 1 : 0 });
    }

    async moveCameraTo(position, orientation) {
        return Promise.all([
            this.moveTo(this._camera_tr.position, position, { onUpdate: getTransformUpdater(this._camera_tr) }),
            this.moveTo(this._camera_tr.orientation, orientation, { onUpdate: getTransformUpdater(this._camera_tr) }),
        ]);
    }

    // async togglingHandPoint(isAppering) {
    //     await this.moveTo(this._handPointMaterial, { opacity: isAppering ? 1 : 0 });
    // }

    moveTo(target, to, { time = 700, onUpdate = () => {} } = {}) {
       return new Promise((resolve) => {
           new TWEEN.Tween(target)
                .to(to, time)
                .onUpdate(onUpdate)
                .onComplete(() => resolve())
                .start();
       });
    }

    _createHandPoint(size = 0.07) {
        const name = 'hand_point';

        const node = this._axe.createChild({
            name: name + '_node',
            position: new GFX.Vector3(0, 0, 1.55),
            scale: new GFX.Vector3(size, size, size)
        });

        const material = new GFX.SpriteMaterial({
            name: name + '_material',
            colorMap: this._resources.get('texture', 'hand_point'),
            alphaMode: GFX.Material.AlphaMode.BLEND
        });

        const component = new GFX.Sprite({ material });
        node.attach(component);

        Object.defineProperty(material._shaders.defines, 'SRGB_OUTPUT', {
            value: 0, writable: false
        });

        material.needsUpdate = true;

        return { node, component, material };
    }

    switchNextColor() {
        this._colorIndex++;

        if (this._colorIndex > this._colorMaps.length - 1) {
            this._colorIndex = 0;
        }

        const colorMaps = this._colorMaps[this._colorIndex];

        this._changeColor(colorMaps);
    }

    switchPrevColor() {
        this._colorIndex--;

        if (this._colorIndex < 0) {
            this._colorIndex = this._colorMaps.length - 1;
        }

        const colorMaps = this._colorMaps[this._colorIndex];

        this._changeColor(colorMaps);
    }

    _changeColor(colorMaps) {
        const material = LIB.getFirstMaterial(this._axe);
        const { albedoMap, metalnessRoughnessMap } = colorMaps;

        material.albedoMap = albedoMap;
        material.ambientOcclusionMap = albedoMap;
        material.metalnessRoughnessMap = metalnessRoughnessMap;
    }

    earlyUpdate() {
        TWEEN.update();

        if (this.progress > 0 && !this._isDropped) {
            this._axeProgressLerper.evaluate(this.progress, this._axe_tr.position);
            // this._axeQuaternionLerper.evaluate(this.progress, this._axe_tr.orientation);
            this._axe_tr.needsUpdateLocalMatrix = true;
            this._axe_tr.needsUpdateWorldMatrix = true;
        }
    }
}
