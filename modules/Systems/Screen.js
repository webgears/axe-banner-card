import Platform from '@webgears-app/platform';
import BaseSystem from './BaseSystem';

const $dom = Symbol.for('dom');
const $onscreen = Symbol.for('onscreen');
let dom = null;
let width = 0;
let height = 0;
let onscreen;

export default class Screen extends BaseSystem {
    constructor(ecs) {
        super(ecs);
        this._width = 1;
        this._height = 1;
    }

    onStart() {
        dom = this._ecs[$dom];
    }

    earlyUpdate() {
        width = Math.min(dom.clientWidth, window.innerWidth);
        height = Math.min(dom.clientHeight, window.innerHeight);
        onscreen = false;

        if (this._width !== width) {
            this._width = width;
            onscreen = true;
        }

        if (this._height !== height) {
            this._height = height;
            onscreen = true;
        }

        this._ecs[$onscreen] = onscreen;
    }

    get width() {
        return this._width;
    }

    get height() {
        return this._height;
    }

    get vertical() {
        return this._height > this._width;
    }

    get mobile() {
        return Platform.mobile;
    }
}
