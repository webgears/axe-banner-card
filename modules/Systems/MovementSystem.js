import * as GFX from '../wg';
import { clamp } from '../Core/lib';
import BaseSystem from './BaseSystem';
import Time from './Time';
import Movement from '../Components/Movement';

let transform = null;
let position = null;
let direction = null;
let maxOffsetX = null;
let speed = 0;
let dt = 0;

export default class MovementSystem extends BaseSystem {
    onStart() {
        this.timeSystem = this._ecs.getSystems([Time])[0];
        this.movementStorage = this._ecs.getStorageOfComponent(Movement);
        this.linearSpeed = new GFX.Vector3();
    }

    earlyUpdate() {
        for (const component of this.movementStorage.components) {
            [transform] = component.owner.getComponents([GFX.Transform]);

            position = transform.position;
            direction = component.direction;
            maxOffsetX = component.maxOffsetX;
            dt = this.timeSystem.deltaTime;
            speed = component.speed * dt;

            position.x += direction.x * speed;
            position.y += direction.y * speed;
            position.z += direction.z * speed;

            if (maxOffsetX) {
                position.x = clamp(position.x, -maxOffsetX, maxOffsetX);
            }

            if (component.linear) {
                position.x += this.linearSpeed.x * dt;
                position.y += this.linearSpeed.y * dt;
                position.z += this.linearSpeed.z * dt;
            }

            transform.needsUpdate = true;
        }
    }

    lateUpdate() {}
}
