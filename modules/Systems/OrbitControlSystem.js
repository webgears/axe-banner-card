import * as GFX from '../wg';
import BaseSystem from './BaseSystem';
import Input from './Input';
import OrbitControl from '../Components/OrbitControl';

const quat = new GFX.Quaternion();
const tv = new GFX.Vector3();

export default class OrbitControlSystem extends BaseSystem {
    onStart() {
        this.inputSystem = this._ecs.getSystems([Input])[0];
        this.orbitControlStorage = this._ecs.getStorageOfComponent(OrbitControl);
        this._enabled = true;
    }

    earlyUpdate() {
        if (!this._enabled) { return; }
        for (const comp of this.orbitControlStorage.components) {
            const [transform] = comp.owner.getComponents([GFX.Transform]);

            if (this.inputSystem.keyState(this.inputSystem.VK_LBUTTON)) {
                if (!comp.pfrozen) comp.polar -= this.inputSystem.mouseDeltaPos().y * comp.speed;
                if (!comp.afrozen) comp.azimuthal -= this.inputSystem.mouseDeltaPos().x * comp.speed;
            } else if (comp.autoRotate && !comp.afrozen) {
                comp.azimuthal -= 0.01;
            }

            transform.orientation.fromRotation(comp.azimuthal, { x: 0, y: 1, z: 0 });
            quat.fromRotation(comp.polar, { x: 1, y: 0, z: 0 });
            transform.orientation.preMultiplyQuaternion(quat);

            comp.forward.set(0, 0, -1);
            comp.forward.applyQuaternion(transform.orientation);
            transform.position.copy(comp.forward);
            transform.position.multiplyNumber(-comp.distance);
            transform.position.add(comp.target);

            tv.copy(comp.center);
            tv.applyQuaternion(transform.orientation);
            transform.position.add(comp.center);
            transform.position.subtract(tv);

            transform.needsUpdateLocalMatrix = true;
            transform.needsUpdateWorldMatrix = true;
        }
    }

    enable() {
        this._enabled = true;
    }

    disable() {
        this._enabled = false;
    }
}
