import * as GFX from '../wg';
import BaseSystem from './BaseSystem';
import Time from './Time';
import Rotation from '../Components/Rotation';

export default class RotationSystem extends BaseSystem {
    onStart() {
        this.timeSystem = this._ecs.getSystems([Time])[0];
        this.rotationStorage = this._ecs.getStorageOfComponent(Rotation);
    }

    earlyUpdate() {
        for (const comp of this.rotationStorage.components) {
            const [transform] = comp.owner.getComponents([GFX.Transform]);

            const quat = new GFX.Quaternion();
            quat.fromRotation(comp.angle * this.timeSystem.deltaTime, comp.axis);

            transform.orientation.preMultiplyQuaternion(quat);
            transform.needsUpdate = true;
        }
    }
}
