import BaseSystem from './BaseSystem';

export default class Time extends BaseSystem {
    constructor(ecs) {
        super(ecs);
        this._time = performance.now();
        this._currentTime = 0;
        this._deltaTime = 0;
        this._timeScale = 1;
        this._focus = true;
    }
    reset() {
        this._time = performance.now();
        this._currentTime = 0;
        this._deltaTime = 0;
        this._timeScale = 1;
    }

    get isFocused() {
        return this._focus;
    }

    onStart() {
        this.reset();
        window.addEventListener('blur', () => { this._focus = false; });
        window.addEventListener('focus', () => { this._focus = true; this._time = performance.now(); });
    }

    earlyUpdate() {
        if (this._focus) this._deltaTime = Math.max((performance.now() - this._time), 0.00001) * this._timeScale * 0.001;
        else this._deltaTime = 0.0000001;
        this._time = performance.now();
        this._currentTime += this._deltaTime;
    }
    lateUpdate() {}

    get time() { return this._currentTime; }
    get deltaTime() { return this._deltaTime; }
    get timeScale() { return this._timeScale; }
    set timeScale(scale) { this._timeScale = scale; }

    static parse(time) {
        let t = time;

        const m = Math.floor(t / 60);
        t -= m * 60;

        const s = Math.floor(t);
        t -= s;

        const ms = Math.floor(t * 1000);

        return [m, s, ms];
    }
}
