import React from 'react';
import ReactDOM from 'react-dom';
import BaseSystem from './BaseSystem';
import GameSystem from './GameSystem';
import Screen from './Screen';
import UI from '../UI/wrapper';

export default class UISystem extends BaseSystem {
    constructor(ecs) {
        super(ecs);

        this._store = {
            mobile: false,
            offsetX: 0,
            isDragging: false,
            isActive: true,
            onTap: () => this._handleTap(),
            onTapEnd: () => this._handleTapEnd(),
            onDrag: (offset) => this._handleDrag(offset),
            onSlideLeft: () => this._handleSlideLeft(),
            onSlideRight: () => this._handleSlideRight()
        };

        this._rootElement = null;
        this._gameSystem = null;
        this._screenSystem = null;
    }

    setStore(data) {
        Object.assign(this._store, data);
    }

    onStart() {
        super.onStart();

        const $uiDom = Symbol.for('uiDom');
        this._rootElement = this._ecs[$uiDom];

        const [gameSystem, screenSystem] = this._ecs.getSystems([GameSystem, Screen]);

        this._gameSystem = gameSystem;
        this._screenSystem = screenSystem;

        ReactDOM.render(
            React.createElement(UI, { store: this._store }),
            this._rootElement
        );
    }

    earlyUpdate() {
        const { mobile } = this._screenSystem;

        this._store.mobile = mobile;
    }

    _handleDrag(offset) {
        this._store.isDragging = true;

        this._gameSystem.progress = offset;
    }

    _handleTap() {
        this._store.opacity = 0.3;
        this._gameSystem.moveToBasketView();
    }

    _handleTapEnd() {
        this._store.opacity = 1;
        this._store.isDragging = false;
        this._store.offsetX = this._gameSystem.tryDrop() ? 1 : 0;
    }

    _handleSlideLeft() {
        this._gameSystem.switchPrevColor();
    }

    _handleSlideRight() {
        this._gameSystem.switchNextColor();
    }
}
