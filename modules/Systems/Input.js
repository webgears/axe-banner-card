import BaseSystem from './BaseSystem';

// const $dom = Symbol.for('dom');

export default class Input extends BaseSystem {
    constructor(ecs) {
        super(ecs);
        this._up = new Uint32Array(8);
        this._down = new Uint32Array(8);
        this._state = new Uint32Array(8);
        this._pos = { x: 0, y: 0 };
        this._dpos = { x: 0, y: 0 };
        this._aup = new Uint32Array(8);
        this._adown = new Uint32Array(8);
        this._astate = new Uint32Array(8);
        this._apos = { x: 0, y: 0 };
        this._dposflag = true;
        this._enabled = false;
    }

    init({ dom }) {
        this._dom = dom;

        dom.setAttribute('tabindex', 1);
        this.enable();

        dom.addEventListener('contextmenu', e => {
            e.preventDefault();
        });
    }

    enable() {
        if (this._enabled) { return; }
        this._enabled = true;

        const dom = this._dom;
        dom.addEventListener('keyup', this._handleKeyUp);
        dom.addEventListener('keydown', this._handleKeyDown);
        dom.addEventListener('mouseup', this._handleMouseUp);
        dom.addEventListener('mousedown', this._handleMouseDown);
        dom.addEventListener('mousemove', this._handleMouseMove);
        dom.addEventListener('touchstart', this._handleTouchStart);
        dom.addEventListener('touchend', this._handleTouchEnd);
        dom.addEventListener('touchmove', this._handleTouchMove);
    }

    disable() {
        this._enabled = false;

        const dom = this._dom;
        dom.removeEventListener('keyup', this._handleKeyUp);
        dom.removeEventListener('keydown', this._handleKeyDown);
        dom.removeEventListener('mouseup', this._handleMouseUp);
        dom.removeEventListener('mousedown', this._handleMouseDown);
        dom.removeEventListener('mousemove', this._handleMouseMove);
        dom.removeEventListener('touchstart', this._handleTouchStart);
        dom.removeEventListener('touchend', this._handleTouchEnd);
        dom.removeEventListener('touchmove', this._handleTouchMove);
    }

    earlyUpdate() {
        for (let i = 0; i < 8; i++) {
            this._up[i] = this._aup[i];
            this._down[i] = this._adown[i];
            this._state[i] = this._astate[i];
            this._aup[i] = 0;
            this._adown[i] = 0;
        }
        if (this._dposflag) {
            this._dpos.x = this._apos.x - this._pos.x;
            this._dpos.y = this._apos.y - this._pos.y;
        } else {
            this._dposflag = true;
            this._dpos.x = 0;
            this._dpos.y = 0;
        }
        this._pos.x = this._apos.x;
        this._pos.y = this._apos.y;
    }

    lateUpdate() {}

    asyncDown(keyCode) {
        const mask = 1 << (keyCode % 32);
        const element = Math.floor(keyCode / 32);
        this._adown[element] |= mask & ~this._astate[element];
        this._astate[element] |= mask;
    }
    asyncUp(keyCode) {
        const mask = 1 << (keyCode % 32);
        const element = Math.floor(keyCode / 32);
        this._aup[element] |= mask;
        this._astate[element] &= ~mask;
    }

    _handleKeyUp = (e) => {
        this.asyncUp(e.keyCode);
    }

    _handleKeyDown = (e) => {
        this.asyncDown(e.keyCode);
        // if (e.keyCode !== 116) e.preventDefault();
    }

    _handleMouseUp = (e) => {
        switch (e.button) {
            case 0: e.keyCode = 1; break;
            case 1: e.keyCode = 4; break;
            case 2: e.keyCode = 2; break;
            default: e.keyCode = 0; break;
        }
        this.asyncUp(e.keyCode);
    }

    _handleMouseDown = (e) => {
        switch (e.button) {
            case 0: e.keyCode = 1; break;
            case 1: e.keyCode = 4; break;
            case 2: e.keyCode = 2; break;
            default: e.keyCode = 0; break;
        }
        this.asyncDown(e.keyCode);
    }

    _handleMouseMove = (e) => {
        this._apos.x = e.clientX;
        this._apos.y = e.clientY;
    }

    _handleTouchStart = (e) => {
        this._apos.x = e.changedTouches[0].clientX;
        this._apos.y = e.changedTouches[0].clientY;
        this.asyncDown(1);
        this._dposflag = false;
    }

    _handleTouchEnd = (e) => {
        this._apos.x = e.changedTouches[0].clientX;
        this._apos.y = e.changedTouches[0].clientY;
        this.asyncUp(1);
    }

    _handleTouchMove = (e) => {
        this._apos.x = e.changedTouches[0].clientX;
        this._apos.y = e.changedTouches[0].clientY;
        e.preventDefault();
    }

    keyUp(key) {
        return (this._up[Math.floor(key / 32)] >> (key % 32)) & 0x1;
    }
    keyDown(key) {
        return (this._down[Math.floor(key / 32)] >> (key % 32)) & 0x1;
    }
    keyState(key) {
        return (this._state[Math.floor(key / 32)] >> (key % 32)) & 0x1;
    }
    mousePos() {
        return this._pos;
    }
    mouseDeltaPos() {
        return this._dpos;
    }

    get VK_LBUTTON() { return 1; }
    get VK_RBUTTON() { return 2; }
    get VK_MBUTTON() { return 4; }
    get VK_XBUTTON1() { return 5; }
    get VK_XBUTTON2() { return 6; }

    get VK_BACK() { return 8; }
    get VK_TAB() { return 9; }

    get VK_ESCAPE() { return 27; }

    get VK_SPACE() { return 32; }
    get VK_PAGEUP() { return 33; }
    get VK_PAGEDOWN() { return 34; }
    get VK_END() { return 35; }
    get VK_HOME() { return 36; }
    get VK_LEFT() { return 37; }
    get VK_UP() { return 38; }
    get VK_RIGHT() { return 39; }
    get VK_DOWN() { return 40; }

    get VK_0() { return 48; }
    get VK_1() { return 49; }
    get VK_2() { return 50; }
    get VK_3() { return 51; }
    get VK_4() { return 52; }
    get VK_5() { return 53; }
    get VK_6() { return 54; }
    get VK_7() { return 55; }
    get VK_8() { return 56; }
    get VK_9() { return 57; }

    get VK_A() { return 65; }
    get VK_B() { return 66; }
    get VK_C() { return 67; }
    get VK_D() { return 68; }
    get VK_E() { return 69; }
    get VK_F() { return 70; }
    get VK_G() { return 71; }
    get VK_H() { return 72; }
    get VK_I() { return 73; }
    get VK_J() { return 74; }
    get VK_K() { return 75; }
    get VK_L() { return 76; }
    get VK_M() { return 77; }
    get VK_N() { return 78; }
    get VK_O() { return 79; }
    get VK_P() { return 80; }
    get VK_Q() { return 81; }
    get VK_R() { return 82; }
    get VK_S() { return 83; }
    get VK_T() { return 84; }
    get VK_U() { return 85; }
    get VK_V() { return 86; }
    get VK_W() { return 87; }
    get VK_X() { return 88; }
    get VK_Y() { return 89; }
    get VK_Z() { return 90; }

    get VK_N0() { return 96; }
    get VK_N1() { return 97; }
    get VK_N2() { return 98; }
    get VK_N3() { return 99; }
    get VK_N4() { return 100; }
    get VK_N5() { return 101; }
    get VK_N6() { return 102; }
    get VK_N7() { return 103; }
    get VK_N8() { return 104; }
    get VK_N9() { return 105; }

    get VK_F1() { return 112; }
    get VK_F2() { return 113; }
    get VK_F3() { return 114; }
    get VK_F4() { return 115; }
    get VK_F5() { return 116; }
    get VK_F6() { return 117; }
    get VK_F7() { return 118; }
    get VK_F8() { return 119; }
    get VK_F9() { return 120; }
    get VK_F10() { return 121; }
    get VK_F11() { return 122; }
    get VK_F12() { return 123; }
    get VK_F13() { return 124; }
    get VK_F14() { return 125; }
    get VK_F15() { return 126; }
    get VK_F16() { return 127; }
    get VK_F17() { return 128; }
    get VK_F18() { return 129; }
    get VK_F19() { return 130; }
    get VK_F20() { return 131; }
    get VK_F21() { return 132; }
    get VK_F22() { return 133; }
    get VK_F23() { return 134; }
    get VK_F24() { return 135; }
}
