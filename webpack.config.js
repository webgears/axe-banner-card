
const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const OfflinePlugin = require('offline-plugin');
const WebpackBar = require('webpackbar');
const glob = require('glob');

const nodeModulesRoot = path.join(__dirname, 'node_modules');
const uiFolder = path.join(__dirname, 'modules/UI');

module.exports = (opts = {}) => {
    const cssLoader = [
        {
            loader: 'style-loader'
        },
        {
            loader: 'css-loader',
            query: {
                sourceMap: !opts.prod,
                modules: true,
                getLocalIdent(context, localIdentName, localName) {
                    const uiPath = path.relative(uiFolder, context.resourcePath);
                    const prefix = 'wg-' + uiPath.replace(/\/index\.s?css/, '').replace('/', '-');

                    return `${prefix}-${localName}`;
                }
            }
        },
        'sass-loader'
    ];

    // For npm link for local development
    const resolve = {
        modules: [nodeModulesRoot, 'node_modules']
    };

    const nodeModulesCssLoader = [
        {
            loader: 'style-loader'
        },
        {
            loader: 'css-loader',
            query: {
                sourceMap: !opts.prod,
                modules: true
            }
        }
    ];

    const plugins = [];

    plugins.push(new webpack.DefinePlugin({
        __wg_worker_pool_no_sharing__: true,
        __wg_worker_pool_max_size__: 1
    }));

    plugins.push(new webpack.LoaderOptionsPlugin({
        debug: !opts.prod,
        options: {
            context: path.join(__dirname, 'modules'),
            eslint: {
                emitWarning: true
            }
        }
    }));

    // if (opts.prod) {
    //     const assetFiles = glob.sync('app/**/*.*').map((filePath) => {
    //         return './' + path.relative('app/', filePath);
    //     });

    //     const externals = [
    //         './',
    //         ...assetFiles
    //     ];

    //     // console.log('Caching external files:', externals);

    //     plugins.push(new OfflinePlugin({
    //         externals,
    //         publicPath: '',
    //         output: './sw.js'
    //     }));
    // }

    plugins.push(new WebpackBar());

    const optimization = {
        minimize: opts.prod,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    ie8: false,
                    ecma: 8,
                    output: {
                        comments: false,
                        beautify: false
                    },
                    compress: {
                        sequences: true,
                        dead_code: true,
                        conditionals: true,
                        booleans: true,
                        unused: true,
                        if_return: true,
                        join_vars: true,
                        drop_console: false
                    },
                    warnings: false
                }
            })
        ]
    };

    const rules = [{
        test: /\.wg-worker-pool-service\.js$/,
        use: '@webgears-app/worker-pool/service-loader'
    }, {
        test: /\.s?css$/,
        loader: cssLoader,
        exclude: nodeModulesRoot
    }, {
        test: /\.sass$/,
        use: [
            'style-loader',
            'css-loader',
            'sass-loader'
        ]
    }, {
        test: /\.s?css$/,
        loader: nodeModulesCssLoader,
        include: nodeModulesRoot
    }, {
        test: /\.(glsl|vert|frag)$/,
        loader: 'webpack-glsl-loader'
    }, {
        test: /\.(gif|jpe?g|png|svg)$/,
        loaders: [
            `url-loader?${JSON.stringify({
                limit: Infinity, // 10 KB
                name: 'images/[name]_[hash:4].[ext]'
            })}`
        ]
    }, {
        test: /\.(otf|ttf|eot|woff2?)$/,
        loader: 'url-loader',
        query: {
            name: 'fonts/[name]_[hash:4].[ext]'
        }
    }];

    rules.push({
        test: /\.jsx?$/,
        loaders: 'babel-loader',
        include: [
            path.join(__dirname, 'modules'),
            path.join(__dirname, 'test/modules/app'),
            path.join(nodeModulesRoot, '@webgears-app')
        ],
        query: {
            presets: [
                '@babel/preset-env',
                '@babel/preset-react'
            ],
            plugins: [
                ['@babel/plugin-transform-runtime', { regenerator: true }],
                ['@babel/plugin-proposal-decorators', { legacy: true }],
                ['@babel/plugin-proposal-class-properties', { loose: true }],
                '@babel/plugin-proposal-export-default-from',
                '@babel/plugin-syntax-dynamic-import',
                'react-hot-loader/babel'
            ]
        }
    });

    return {
        devServer: {
            hot: !opts.prod,
            contentBase: 'app',
            disableHostCheck: true,
            stats: {
                colors: true,
                chunkModules: false,
                chunks: false,
                hash: false,
                version: false
            }
        },
        entry: {
            app: [
                // './modules/pwa',
                './modules/App'
            ]
        },
        resolve,
        output: {
            path: path.join(__dirname, 'dist'),
            publicPath: '',
            filename: '[name].js',
            library: 'GfxEngineApp',
            chunkFilename: '[name].js',
            globalObject: 'this' // For HMR
        },
        plugins,
        optimization,
        module: {
            rules
        },
        performance: {
            hints: false
        },
        devtool: opts.prod ? false : 'source-map',
        mode: opts.prod ? 'production' : 'development'
    };
};
