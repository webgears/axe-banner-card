const path = require('path');
const createTestCafe = require('testcafe');
const browsers = require('./browsers/babel');

let testcafe = null;

/*
* This runner is needed for more convenient than in cmd way browser list setup */

createTestCafe('localhost', 1337, 1338)
    .then((tc) => {
        testcafe = tc;

        return testcafe.createRunner()
            .src(path.resolve(__dirname, '../tests/**/*.js'))
            .screenshots(path.resolve(__dirname, '../../../screenshots'))
            .browsers(browsers)
            .reporter('teamcity')
            .run();
    })
    .then((failedCount) => {
        console.log('Tests failed: ' + failedCount);
        testcafe.close();
    });
