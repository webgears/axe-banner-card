module.exports = [
    'browserstack:safari@12.0:OS X Mojave',
    'browserstack:safari@11.1:OS X High Sierra',
    'browserstack:safari@10.1:OS X Sierra',
    'browserstack:edge@17.0:Windows 10',
    'browserstack:iPhone 8@11.0',
    'browserstack:iPhone 7 Plus@10.3',
    'browserstack:Samsung Galaxy Note 9@8.1',
    'browserstack:Samsung Galaxy S9@8.0'
];
