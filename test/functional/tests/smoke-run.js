import { ClientFunction, Selector } from 'testcafe';
import { canvasIsEmpty } from 'wg-testcafe-utils';

import { baseUrl } from '../config';

fixture('Test scene')
    .page(baseUrl);

const canvas = Selector('.canvas');

const appReady = ClientFunction(() => {
    return window.runTestReady;
});

test('Can rotate without errors', async t => {
    await appReady();
    await t.drag(canvas, 100, 100);
});

// disable canvas test before this issue resolved
// https://github.com/DevExpress/testcafe-browser-provider-browserstack/issues/51
if (!process.env.TEST_IGNORE_CANVAS_TEST) {
    test('Canvas is not empty', async t => {
        await appReady();
        const isEmpty = await canvasIsEmpty(t, Selector('.canvas canvas'));
        return t.expect(isEmpty).eql(false, 'canvas is empty');
    });
}
