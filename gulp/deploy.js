
const path = require('path');
const _ = require('lodash');
const rsync = require('gulp-rsync');
const pkg = require('../package.json');

const pkgBaseName = _.last(pkg.name.split('/'));

module.exports = (gulp, globOpts = {}) => {
    const prefix = globOpts.prefix ? `${globOpts.prefix}-` : '';
    const postfix = globOpts.postfix ? `-${globOpts.postfix}` : '';

    const name = `${prefix}${pkgBaseName}${postfix}`;

    const wgdemosPath = './';

    function deploy(opts = {}) {
        const localPath = path.join(wgdemosPath, opts.prod ? '' : 'dev3d', name);
        const sshPath = path.join('/var/www/', localPath);

        return gulp.src('dist/**')
            .pipe(rsync({
                root: 'dist',
                username: 'wgdemos',
                hostname: 'b3.webgears3d.com',
                incremental: true,
                recursive: true,
                clean: true,
                destination: sshPath
            }))
            .on('end', () => {});
    }

    return {
        run: deploy
    };
};
