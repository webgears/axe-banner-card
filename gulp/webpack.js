/* eslint-disable no-param-reassign */

const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');

const gutil = require('gulp-util');
const ip = require('ip');

function runServerOnPort(config, port) {
    const localServer = `http://${ip.address()}:${port}`;

    const { entry, output } = config;

    entry.app.unshift(
        'react-hot-loader/patch',
        `webpack-dev-server/client?${localServer}`
        // 'webpack/hot/only-dev-server'
    );

    // output.publicPath = localServer + '/';

    const server = new WebpackDevServer(webpack(config), config.devServer);

    server.listen(port, (err) => {
        if (err) { throw new gutil.PluginError('webpack', err); }

        gutil.log('Listening', gutil.colors.magenta(localServer));
    });
}

function runServer(config) {
    const portfinder = require('portfinder');
    portfinder.basePort = 9000;
    portfinder.getPort((err, port) => {
        if (err) { throw new gutil.PluginError('webpack', err); }

        runServerOnPort(config, port);
    });
}

function webpackTask(cb, opts) {
    opts = opts || {};
    cb = cb || function() {};

    const config = require('../webpack.config.js')(opts);

    if (opts.watch) {
        return runServer(config);
    }

    webpack(config, (err, stats) => {
        if (err) { throw new gutil.PluginError('webpack', err); }

        const formatErrs = key => stats.toJson()[key].join('\n');

        if (stats.hasErrors()) {
            return cb(new gutil.PluginError('webpack', formatErrs('errors')));
        }

        if (stats.hasWarnings()) {
            console.log('Webpack Warnings:');
            console.warn(formatErrs('warnings'));
        }

        if (opts.stats) {
            const fs = require('fs');
            const json = JSON.stringify(stats.toJson(), null, 4) + '\n';
            fs.writeFile('stats.json', json, cb);
            return;
        }

        cb();
    });
}

module.exports = (/* gulp */) => {
    return { run: webpackTask };
};
