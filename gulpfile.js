/* eslint-disable no-param-reassign */

const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');

const $ = gulpLoadPlugins();

const del = require('del');

const webpack = require('./gulp/webpack')(gulp);
const deploy = require('./gulp/deploy')(gulp);

gulp.task('clean', (cb) => del(['dist'], cb));

gulp.task('html', ['clean'], () => {
    return gulp.src('app/*.html')
        .pipe($.useref())
        .pipe(gulp.dest('dist'));
});

gulp.task('copy:assets', ['clean'], () => {
    return gulp.src('app/assets/**/*')
        .pipe(gulp.dest('dist/assets'));
});

gulp.task('extras', ['clean'], () => {
    return gulp.src([
        'app/**',
        '!app/*.html'
    ], {
        dot: true
    }).pipe(gulp.dest('dist'));
});

gulp.task('serve', (cb) => webpack.run(cb, { watch: true }));

gulp.task('gzip', ['webpack:prod', 'html', 'copy:assets'], () => {
    return gulp.src('dist/**/*.*(js|json|gltf|bin)')
        .pipe($.gzip({ gzipOptions: { level: 9 } }))
        .pipe(gulp.dest('dist'));
});

gulp.task('webpack', (cb) => webpack.run(cb));
gulp.task('webpack:prod', (cb) => webpack.run(cb, { prod: true }));
gulp.task('webpack:stats', (cb) => webpack.run(cb, { prod: true, stats: true }));

gulp.task('build', ['html', 'extras', 'copy:assets', 'webpack:prod', 'gzip']);

gulp.task('publish', ['build'], () => deploy.run({}));
gulp.task('publish:prod', ['build'], () => deploy.run({ prod: true }));

gulp.task('deploy', () => deploy.run({}));
gulp.task('deploy:prod', () => deploy.run({ prod: true }));
