## Install
```
npm i
```

## Run
```
gulp serve
```
babel
```
gulp serve --babel
```

## Build
```
gulp build
```
babel
```
gulp build --babel
```

## Tests
```
gulp serve
// in other console
npm run test-dev
```

